/***************************************************************
 * project : rat4 (quad CV LFO)
 * target  : Atmega328
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 *
 * ./qusw
 ****************************************************************/
#include "inc/mcp4922.h"

#include "inc/serialTx.h"
#ifndef F_CPU
  #define F_CPU 16000000UL
#endif

/* Bit Mask
   bit 15 : 0 >> channel A ; 1 >> channel B
   bit 14 : 0 >> unbuffered (not using LDAC)
   bit 13 : 1 >> Vout = Vref*D/4096
   bit 12 : 1 >> output enabled
*/
const uint8_t SPI_MASK = 0b00110000;

void init_spi(void){
  SPI_DDR |= (1<<SCK_PIN) | (1<<MOSI_PIN);
  SPI_DDR &= ~(1<<MISO_PIN);
  SPI_DDR |= (1<<SS_PIN);
  /* Warning: if SS=PB2 must be configured as output even if not used
     if not, SPI will eventually become slave */
  SPI_DDR |= (1<<CS1_PIN) | (1<<CS2_PIN); // Chip Select as outputs
  SPI_PRT |= (1<<CS1_PIN) | (1<<CS2_PIN); // Chip Select high by default
  SPCR |= (1<<MSTR)|(1<<SPE);   // Enable SPI as Master
  SPCR |= (1<<SPR0);//|(1<<SPR1);  // clock
}

void send_spi_byte(uint8_t b){
  SPDR = b; //start transmission
  while(!(SPSR & (1<<SPIF))); //wait for transmission complete
}

void send_spi_int(uint16_t data, uint8_t csPin, uint8_t channel){
  SPI_PRT &= ~(1<<csPin);
  uint8_t MSB = (uint8_t)(data>>8);
  MSB = (channel<<7)|(SPI_MASK)|MSB;
  /* stx_str("\r\nMSB:"); */
  /* stx_int(MSB); */
  uint8_t LSB = (uint8_t)(data);
  /* stx_str("\r\nLSB:"); */
  /* stx_int(LSB); */
  send_spi_byte(MSB);
  send_spi_byte(LSB);
  SPI_PRT |= (1<<csPin);
}
