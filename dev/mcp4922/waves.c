/****************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328P (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * Functions to generate/update LFO waves (see waves.c)
 *
 **********************************************************************/

#include "inc/waves.h"
uint16_t sine_wave(uint8_t i){
  uint16_t value = sinTable[i];
  return value;
}
