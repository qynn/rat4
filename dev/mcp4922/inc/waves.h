/****************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328P (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * Functions to generate/update LFO waves (see waves.c)
 *
 **********************************************************************/

#ifndef WAVES_H
#define WAVES_H

#include <stdio.h>
#include <avr/io.h>
#include "sinTable.h"

/* static const uint16_t OUTPUT_AMP = 2048; */

uint16_t sine_wave(uint16_t i);

#endif /*WAVES_H*/
