/****************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328P (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * ./qusw
****************************************************************/

#include <util/delay.h>
#include "inc/mcp4922.h"
#include "inc/serialTx.h"
#include "inc/waves.h"

uint16_t i=0;

int main(){
  init_TX0();
  stx_str("spi welcome");
  init_spi();

	while(1){
    uint16_t val = sine_wave(i);
    send_spi_int(val,CS1_PIN,SPI_CHA);
    /* stx_int(i); */
    /* stx_str(":"); */
    /* stx_int(val); */
    /* stx_str("\r\n"); */
    _delay_us(1);
    if(++i==TABLE_SIZE){i=0;}
  }
}
