#include "incl/blink.h"
#include "incl/utils.h"
#include <avr/io.h>

//#include <stdlib.h>
//#include <stdint.h>
//#include <util/delay.h>

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define DDR DDRD
#define PORT PORTD
#define PIN 1

int main(void)
{
    DDR |= (1<<PIN);
    while(1)
    {
        // LED on
        PORT |= (1<<PIN);
        long_delay(2);
        //_delay_ms(500);

        //LED off
        PORT &= ~(1<<PIN);
        long_delay(3);
        //_delay_ms(500);
    }
}
