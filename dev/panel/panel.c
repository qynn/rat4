/*******************************************************************
 * file    : panel.c
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * Functions for reading Pot/Switch values on front panel
 *
 * ./qusw
********************************************************************/

#include "inc/panel.h"

/******* Rat Pots ************/

/* const uint8_t R347_pot[11]={ */
/* 	0,5,25,58,88, */
/* 	117,140,167, */
/* 	198,229,251}; */
/* const uint8_t R347_val[11]={ //retrieve 12 */
/* 	99,0,3,4,7, */
/* 	12,15,16,19, */
/* 	24,27}; */


uint8_t get_rat(uint8_t i){
	set_MPX(i); // Rat pots id = ADC channel
	uint8_t p=read_ADC_8bit(); //0-255
  uint8_t j = (p>>5); // 1/8 pot resolution
  float r = (p+1)/32.0 - j;
  float interp = (RAT_96PPQN[j+1] - RAT_96PPQN[j])*r;
  uint8_t ret = RAT_96PPQN[j] + (uint8_t)interp;
	/* uint8_t ret=(pot>>3); // 1/32 pot. resolution (256(8-bit)/8 = 32) */
	return ret;
}


/******* Push Buttons  ************/

void init_pb(void){ // set PB pins as input
	DDR_PB &= ~(1<<PB_1) & ~(1<<PB_2) & ~(1<<PB_3) & ~(1<<PB_4);
}

uint8_t check_pb(uint8_t pbPin){
	if ( !(PRT_PB & (1<<pbPin)) ){  //Pin to GND
		return 0;
  }
	else{ return 1;}
}

/******* Mode Switch *********/

void init_sw(void){
  DDR_SW &= ~(1<<SW_M);
}

uint8_t check_sw(void){ // check Mode Switch
	if ( !(PRT_SW & (1<<SW_M)) ){  //Pin to GND
		return 0;
  }
	else{ return 1;}
}


/******* Wave Select *********/

uint8_t get_wave(void){
	set_MPX(CH_W);
	uint8_t pot=read_ADC_8bit(); //0-255
	uint8_t wav=(pot>>4); // 1/8 pot. resolution (256(8-bit)/2^5=8)
	return (wav+1)/3;
  }

/* test Alt+j
 * this is
 * a comment
 * obviously
 */

void init_panel(void){
  init_ADC();
  init_pb();
  init_sw();

}
