/****************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328P (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * ./qusw
****************************************************************/

#include "inc/rat4.h"
#include <util/delay.h>
/* #include <avr/eeprom.h> */

/*** Rate/Phase Knobs ***/
uint8_t rat1 = 0;
uint8_t rat2 = 0;
uint8_t rat3 = 0;
uint8_t rat4 = 0;

/*** TIMING parameters (c.f. TIMER0 (100kHz) in  clock.c) ***/
volatile uint16_t clk; // number of ticks since begining of step.
uint16_t Tclk=3000;  // pulse length in microseconds (us)
uint8_t Ntck = 200; // number of ticks per step
uint8_t step = 0;

int main(){

	/* init_ADC();  //Analog to Digital Converters */
	init_TX0();	// Hardware Serial Tx
  init_clk(); // Timer0 @100kHz
	init_panel();	// Knobs/Switches

  stx_str("\n\r rat4 welcome");

	while(1){
		if(clk > Ntck){ //New Step reached
      Tclk = get_tclk();
      stx_str("\tTclk:");
      stx_int(TCLK_MIN);
      stx_str("<");
      stx_int(Tclk);
      stx_str("<");
      stx_int(TCLK_MAX);
			clk = clk-Ntck;	//update clock
      stx_str("\n\r rat");

      for(uint8_t r=1; r<=4; r++){
        uint8_t rat = get_rat(r);
        stx_str("\t");
        stx_int(r);
        stx_str(":");
        stx_int(rat);
      }

      uint8_t m = check_sw();
      stx_str("\n\r mode:");
      stx_int(m);

      uint8_t w = get_wave();
      stx_str("\n\r wave:");
      stx_int(w);

      stx_str("\n\r push");
      for(uint8_t p=0; p<4; p++){
        uint8_t sw = check_pb(PB_i[p]);
        stx_str("\t");
        stx_int(p+1);
        stx_str(":");
        stx_int(sw);
      }

			if(++step == 8){ //increment
				step=0; //init (end of line)
      }
    }
  }
}
