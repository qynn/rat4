/****************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328 (20MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * CLOCK generation
 *
 * ./qusw
****************************************************/

#include "inc/clock.h"
#include "inc/panel.h"

#ifndef F_CPU
  #define F_CPU 16000000UL
#endif

/* -----INTERNAL CLOCK-------*/

volatile uint16_t t=0;   // time in microseconds
extern volatile uint16_t Tclk;  //Pulse length in microseconds
extern volatile uint16_t clk; // number of ticks since last step

void init_clk(void){ /* Set up TIMER0 at fHz=100kHz (CTC mode) */
	/*  F_CPU = 16MHz ; fHz=100kHz ; Pre = 1
	 * 	OCR0A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(100*10^3*1) -1] = 159 <256!!
	 */
  cli(); 						// disable global interrupts
  TCCR0A = 0;					// set entire TCCR0A register to 0
	TCCR0B = 0;					// same for TCCR0B
	TCNT0  = 0;					// initialize counter value to 0
	OCR0A = 159;  				// set Compare Register (see comment above)
	TCCR0A |= (1 << WGM01);		// clear timer on compare match (CTC mode on)
	TCCR0B |= (1 << CS00);   	// no prescaling
	TIMSK0 |= (1 << OCIE0A); 	// enable interrupts on TIMER0 (COMPA)
	sei();						// enable global interrupts
}

ISR(TIMER0_COMPA_vect){ //TIMER0 @ 100kHz (dt=0.01ms=10us)
	t+=10; // (us)
	if(t>Tclk){ // tick received
		clk++;
		t=0;
	}
}

uint16_t get_tclk(void){
  set_MPX(CH_C);
  uint16_t pot=read_ADC_10bit();
  uint16_t Tclk=(uint16_t)(TCLK_MAX-1.0*pot*(TCLK_MAX-TCLK_MIN)/1023.0);
  return Tclk;
}
