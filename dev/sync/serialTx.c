/***************************************************************
 * project : rat4 (quad CV LFO)
 * target  : Atmega328
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * Serial UA(R)T (TX0) for Atmega328 (16MHz)
 *
 * ./qusw
 ****************************************************************/
#include "serialTx.h"

#ifndef F_CPU
  #define F_CPU 16000000UL
#endif

#define BAUD 115200
// BaudRate Expression for double speed Mode :
// UBBR = FCPU/8BAUD -1 +0.5 (rounded)
//#define UBRRV (( (F_CPU) + 4UL * (BAUD) ) / (8UL * (BAUD) ) - 1UL)

// BaudRate Expression for normal asynchronous Mode :
// UBBR = FCPU/16BAUD -1 +0.5 (rounded)
#define UBRR0V (( (F_CPU) + 8UL * (BAUD) ) / (16UL * (BAUD) ) - 1UL)
/*alternately check  #include <util/setbaud.h> */

void init_TX0(void) {
    UBRR0H = (UBRR0V>>8);     			// UBBRV MSB
    UBRR0L = (UBRR0V& 0xff); 			// UBBRV LSB
    //UCSR0A |= (1<<U2X0); 			// using 2X speed
    UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00); 	// 8N1 (sync) mode
    UCSR0B |= (1<<TXEN0);   			// enable TX0

}

void stx_byte(uint8_t b) {
    wait_for_flag(UCSR0A, UDRE0); /* Wait until data register is empty. */
    UDR0 = b;
} /*alternately wait until transmission is ready: wait_for_flag(UCSR1A,TXC1)*/


void stx_int(int a){
    uint8_t lgth=6;
    char buf[lgth]; //5 digits + nul character ('\0')
    char *str=&buf[lgth-1]; //now points to last character
    *str ='\0'; //nul character to terminate string
    do{
        char c = a % 10;
        *(--str)= c + '0'; //ascii digits start at '0'=48;
        a/=10;
    }while(a);

    stx_str(str);
}

void stx_str(char *s){
    uint8_t i= 0; //counter

    stx_byte('\0'); //send dummy byte to avoid errors on first character
    while ( (s[i] != '\0') & (i < STR_MAX) ) //end of string
    {
        stx_byte(s[i]);
        i++;
    }
}
