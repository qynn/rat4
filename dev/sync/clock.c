/****************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328 (20MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * CLOCK generation
 *
 * ./qusw
****************************************************/

#include "inc/clock.h"
#include "inc/panel.h"

#ifndef F_CPU
  #define F_CPU 16000000UL
#endif

/* external clock */
extern volatile uint8_t extClkOn;    // 1 if CV clock is active
uint8_t extClkTimeout; // increments when CV clock becomes inactive


/*general purpose timer*/
volatile uint16_t t_ms=0;   // time in milliseconds

/* internal clock */
volatile uint16_t t_us=0;   // time in microseconds
extern volatile uint16_t Tclk;  //Pulse length in microseconds
extern volatile uint16_t clk; // Clock counter

extern volatile uint8_t sync;

void init_clk(void){ /* Set up TIMER0 at fHz=100kHz (CTC mode) */
	/*  F_CPU = 16MHz ; fHz=100kHz ; Pre = 1
	 * 	OCR0A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(100*10^3*1) -1] = 159 <256!!
	 */
  cli(); 						// disable global interrupts

  TCCR0A = 0;					// set entire TCCR0A register to 0
	TCCR0B = 0;					// same for TCCR0B
	TCNT0  = 0;					// initialize counter value to 0
	OCR0A = 159;  				// set Compare Register (see comment above)
	TCCR0A |= (1 << WGM01);		// clear timer on compare match (CTC mode on)
	TCCR0B |= (1 << CS00);   	// no prescaling
	TIMSK0 |= (1 << OCIE0A); 	// enable interrupts on TIMER0 (COMPA)

  //set external interrupt on CLOCK=INT0=PD2  & SYNC=INT1=PD3
	DDRD &= (~(1<<2)) &  (~(1<<3));  	// PD2 & PD3 as inputs
  EICRA |= (1<<ISC01); 	// set INT0 as falling edge
  EIMSK |= (1<<INT0); 						//turn on INT0='D2'
  EICRA |= (1<<ISC11); 	// set INT1 as falling edge
  EIMSK |= (1<<INT1); 						//turn on INT1='D3'

  sei();						// enable global interrupts
}

void init_timer(void){ /* Set up TIMER1(16-bit) at fHz=1kHz (CTC mode) */
	/*  F_CPU = 16MHz ; fHz=1000Hz ; Pre = 1
	 * 	OCR0A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(1*10^3*1) -1] = 15999 < 65535!!
	 */
  cli(); 						// disable global interrupts

  TCCR1A = 0;					// set entire TCCR1A register to 0
	TCCR1B = 0;					// same for TCCR1B
	TCNT1  = 0;					// initialize counter value to 0
	OCR1A = 15999;  				// set Compare Register (see comment above)
	TCCR1A |= (1 << WGM01);		// clear timer on compare match (CTC mode on)
	TCCR1B |= (1 << CS10);   	// no prescaling
	TIMSK1 |= (1 << OCIE1A); 	// enable interrupts on TIMER1 (COMPA)

  sei();						// enable global interrupts
}

ISR(TIMER1_COMPA_vect){ //TIMER1 @ 1kHz (dt=1ms)
	t_ms+=1; // (ms)
}

ISR(TIMER0_COMPA_vect){ //TIMER0 @ 100kHz (dt=0.01ms=10us)
	t_us+=10; // (us)

  if(extClkOn & (t_us>TCLK_MAX) ){
    if(++extClkTimeout==EXT_CLK_TIMEOUT){
      extClkOn=0;
      extClkTimeout=0;
    }
  }
	else if(!extClkOn & (t_us>Tclk)){ // internal clock tick
		clk++;
		/* t_us=0; */
		t_us-=Tclk;
	}
}

/******* Internal Clock Pulse Length*********/
uint16_t get_tclk(void){
  set_MPX(CH_C);
  uint16_t pot=read_ADC_10bit();
  uint16_t Tclk=(uint16_t)(TCLK_MAX-1.0*pot*(TCLK_MAX-TCLK_MIN)/1023.0);
  return Tclk;
}

/******* Clock Multiplier*********/
uint8_t get_Xclk(void){
  set_MPX(CH_C);
  uint8_t pot = read_ADC_8bit();
  uint8_t ret = (pot >>5); // 1/8 pot/ resolution 
  return ret;
}

ISR(INT0_vect){ //falling edge INT0=CLOCK
  extClkOn=1;
  t_us=0;
  /* clk++; */
  clk=201; //testing w/ slow arpegiattor gate
}

ISR(INT1_vect){ //falling edge INT1=SYNC
  t_us=0;
  clk=0;
  sync++;
}
