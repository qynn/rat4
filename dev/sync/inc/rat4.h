/**************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328 (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * ./qusw
***************************************************************/
#ifndef RAT4_H
#define RAT4_H

#define F_CPU 16000000UL

#include "clock.h"
#include "serialTx.h"
#include "panel.h"

#define ON 1
#define OFF 0



#endif /*RAT4_H*/
