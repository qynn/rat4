/*****************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328 (20MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * ./qusw
******************************************************************/
#ifndef CLOCK_H
#define CLOCK_H


#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>

/* -----INTERNAL CLOCK-------*/
#define PPQN 96 // Pulses per Quarter Note
#define TEMPO_MIN	30 // BPM
#define TEMPO_MAX	160 // BPM
static const uint16_t TCLK_MIN=(15000000/(TEMPO_MAX*PPQN)); // us
static const uint16_t TCLK_MAX=(15000000/(TEMPO_MIN*PPQN)); // us
static const uint8_t EXT_CLK_TIMEOUT=254;

void init_clk(void);

uint16_t get_tclk(void);

#endif /*CLOCK_H*/
