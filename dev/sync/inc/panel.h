/********************************************************************
 * project : rat4 (quad CV LFO)
 * target  : ATmega328P (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 * Functions for reading Pot/Switch values on front panel
 *
 * ./qusw
********************************************************************/

#ifndef PANEL_H
#define PANEL_H

#include <stdint.h>
#include "clock.h"
#include "adcmux.h"

// Potentiometers
#define CH_C 0  // (C) Clock
#define CH_1 1	// (1) Rat1
#define CH_2 2	// (2) Rat2
#define CH_3 3	// (3) Rat3
#define CH_4 4	// (4) Rat4
#define CH_W 5	// (W) Wave

#define POT_RES 8 // 256(8-bit)/32

// Push-Buttons
#define PRT_PB PIND
#define DDR_PB DDRD
#define PB_1 4  // Start1 (S1)
#define PB_2 5	// Start2 (S2)
#define PB_3 6	// Start3 (S3)
#define PB_4 7	// Start4 (S4)
static const uint8_t PB_i[4]={PB_1,PB_2,PB_3,PB_4};

// ON-OFF Mode Switch
#define PRT_SW PINB
#define DDR_SW DDRB
#define SW_M 4 // Mode (S5)
/*
Warning! S5=PB4 is shared w/ MISO pin (input only)
avrdude cannot flash if Mode switch is pulling S5 to GND
must keep mode switch lever up before programming
 */

static const uint8_t RAT_96PPQN[9] = {8,12,16,24,32,48,64,96,128};

uint8_t get_rat(uint8_t i);

void init_panel(void);

void init_pb(void);

uint8_t check_pb(uint8_t pbPin);

void init_sw(void);

uint8_t check_sw(void);

uint8_t get_wave(void);

#endif /* PANEL_H */
