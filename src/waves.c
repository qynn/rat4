/****************************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Functions to generate/update LFO waves (see waves.c)
*
**********************************************************************/

#include "inc/waves.h"

uint16_t map_up(uint16_t i, uint16_t imax){
  return (uint16_t)((1.0*i*WAVE_AMP_MAX)/(1.0*imax));
}

uint16_t map_dwn(uint16_t i, uint16_t imax){
  return WAVE_AMP_MAX - map_up(i,imax);
}

uint16_t sine(uint16_t i){
  return sinTable[i];
}

uint16_t square(uint16_t i){
  return ( (i>WAVE_TABLE_SIZE/2) ? 0 : WAVE_AMP_MAX );
}

uint16_t squircle(uint16_t i){
  return sinTable[i]; //using sine for now
}

uint16_t saw_up(uint16_t i){
  return map_up(i,WAVE_TABLE_SIZE);
}

uint16_t saw_dwn(uint16_t i){
  return map_dwn(i,WAVE_TABLE_SIZE);
}

uint16_t triangle(uint16_t i){
  uint16_t w_2 = WAVE_TABLE_SIZE/2;
  return ( (i>w_2) ? map_dwn(i-w_2, WAVE_TABLE_SIZE-w_2) : map_up(i,w_2) );
}

uint16_t noise(void){
  return (uint16_t)(WAVE_AMP_MAX*1.0*rand()/RAND_MAX);
}

uint16_t off(void){
  return WAVE_AMP_MAX/2;
}

uint16_t hold(uint16_t sample){
  return sample;
}

//holds sample for half a period then comes back to zero
uint16_t hump(uint16_t i, uint16_t sample){
  return ( (i>WAVE_TABLE_SIZE/2) ? WAVE_AMP_MAX/2 : sample );
}

// glides to zero from random sample
uint16_t glide(uint16_t i, uint16_t sample){
  uint16_t w_2 = WAVE_TABLE_SIZE/2;
  uint16_t ret;
  float delta = (float)sample - WAVE_AMP_MAX/2.0;
  if(i>w_2){
    ret = (uint16_t)( 1.0*sample - (i-w_2)*delta/(1.0*w_2) );
  }else{
    ret = (uint16_t)( WAVE_AMP_MAX/2.0 + i*delta/(1.0*w_2) );
  }
return ret;
}
