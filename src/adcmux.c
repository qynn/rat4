/****************************************************************
* file    : adcmux.c
* project : qr347 (8-step Midi Sequencer)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Minimal AnalogRead() for ATmega328P
*
****************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>

void init_ADC(void){

/********NB**********
 *   The ATmega328P's ADC is connected to a 8-channel analog multiplexer
 * 	(ADC0 to ADC7)
 ***PRESCALER
 *  ADC clock frequency must be between 50kHz and 200kHz
 *  ADC prescaler must be set accordingly
 *  F_CPU=16MHz ; Pre=128 >> 125kHz
 *  A conversion takes 13 ADC clock cycles (idle)
 *  13/125kHz= 104 microseconds (~ 9,6kHz)
 *
 ***RESOLUTION
 * ADLAR= 1 enables the Left-shift result presentation
 * 		ADCH |b9|b8|b7|b6|b5|b4|b3|b2|
 * 		ADCL |b1|b0|--|--|--|--|--|--|
 * This way, if no more than 8-bit precision is required
 * it is sufficient to read ADCH (b9 to b2)
 * Otherwise one must read ADCL first then ADCH
 *
 ***REF. VOLTAGE
 * By default REFS1=0; REFS2=0; in ADMUX register
 * reference voltage is then set  to VCC
 *
 ***INTERRUPT
 * Setting ADIE=1 enables the ADC Conversion Complete Interrupt
 * so one can use the ADC Interrupt Servide Routine : ISR(ADC_vect){ }
 *
 */
	cli();
	ADMUX = 	(1 << ADLAR) |    	// left-shift result for 8-bit readings
				(1 << REFS0) | 		//AVCC with external capacitor at AREF pin
				(1 << MUX0)  |      // start with ADC3
				(1 << MUX1);

	ADCSRA = 	(1 << ADEN)  | //(1 << ADIE) | 					//enables ADC WHITHOUT interrupts
				(1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)  ;   //set Prescaler at 128

	DDRC=0; //init Data Direction Register for all ADC pins
	sei();
}

void set_MPX(uint8_t channel){

	//ADCSRA &= ~(1<<ADEN); //switch off ADC


	ADMUX = (1 << ADLAR) | (1 << REFS0) | ((0x0f) & channel);  //select ADC channel

	/* ADMUX = |REFS1|REFS0|ADLAR| - |MUX3|MUX2|MUX1|MUX0|
	 *
	 * single-ended input (MUX3=0)
	 * AVCC reference voltage (REFS1=0; REFS0=1)
	 * left-shifted presentation (ADLAR =1);
	 */

	//ADCSRA|=(1<<ADEN); // re-enable ADC
}

uint8_t read_ADC_8bit(void){

		ADCSRA|=(1<<ADSC);  //start new conversion

		/* ADSC==1 as long as a conversion is in process */

		while (ADCSRA & (1<<ADSC));//wait until conversion is complete

	return ADCH;
}

uint16_t read_ADC_10bit(void){

		ADCSRA|=(1<<ADSC);  //start new conversion

		/* ADSC=1 as long as a conversion is in process */

		while (ADCSRA & (1<<ADSC)); //wait until conversion is complete

		/*NB: left-shifted data (ADLAR=1)
		 * ADCH |b9|b8|b7|b6|b5|b4|b3|b2|
		 * ADCL |b1|b0|--|--|--|--|--|--|
		 */

	uint16_t lsb=(uint16_t)ADCL; // ! ADCL must be read first !
	uint16_t msb=(uint16_t)ADCH;
	return ( (msb<<2)| (lsb>>6) );
}
