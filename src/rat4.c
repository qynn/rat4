/****************************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
****************************************************************/

#include "inc/rat4.h"
/* #include <util/delay.h> */
/* #include <avr/eeprom.h> */

#include <stdio.h>

struct Rat rat[4];

/*** CLOCK parameters (c.f. TIMERS in  clock.c) ***/
uint32_t Tclk = 3000;  // pulse length in microseconds (us)
uint8_t extClkOn = 0;    // 1 if CV clock is active
uint8_t Xclk = INT_XCLK; // clock multiplier

/*** PANEL ***/
extern volatile uint16_t t_ms; //time in ms
uint8_t p = 0;  // panel increment id [0-3]
uint8_t m = 0 ; // mode index (rate/phase)

uint8_t c = 0; // conductor index (master rate/zero phase)
uint16_t data_h = 0; // sample to hold/glide to

void check_butt(uint8_t j){ // push buttons
    if(check_pb(j)){ // button j pushed
      if(++rat[j].push == PUSH_LONG){ //long press
        m =!m; //togggle mode
        c = j; // rat[j] becomes conductor
        if(DBG_PRT){
        printf("\nNEW MODE: m=%i (c=%i)",m,j);
        }
        rat[j].push = 0;
        if(m){PRT_LED |= (1<<PIN_LED);} //LED on
        else{PRT_LED &= ~(1<<PIN_LED);} //LED off
      }
      rat[j].wave = get_wave(); // read slider
      rat[j].cntr = 0; // reset table counter
    }
    else{rat[j].push = 0;} //reset push counter
}

void check_rat(uint8_t p){ // potentiometers
  uint16_t  t_ms_cpy = t_ms; //copy volatile
  if( (t_ms_cpy - rat[p].lastPoll) > PANEL_POLL_DELAY){
    Xclk = ( extClkOn ? get_Xclk() : INT_XCLK); // clock multiplier
    Tclk = ( extClkOn ? Tclk : get_Tclk() ); // int. clock Delay(us)
    rat[p].Ntck = ( ((p==c)|!m) ?  get_Ntck(p,Xclk) : rat[c].Ntck ); // cryptic
    /* conductor sets the rate in phase-mode and the zero phase in rate-mode */
    rat[p].incr = 1.0*WAVE_TABLE_SIZE/(float)rat[p].Ntck;
    rat[p].zero = ( ((p==c)|!m) ? 0 : get_Zero(p) ); // phase value
    uint16_t di10 = (uint16_t)(10.0*rat[p].incr);
    /* uint8_t Xex = get_Xclk(); */
    uint16_t zi = (uint16_t)(rat[p].zero);

    if(DBG_PRT & (p==0)){
      printf("\n p_%i\
 N:%3i\
 zi:%3i\
 di:%3i/o\
 C:%i\
 c:%i\
 X:%i\
 T:%lu\
 m:%i\
 w:%i", p, rat[p].Ntck, zi, di10, extClkOn, (int)rat[p].cntr, Xclk, Tclk, m, rat[p].wave);;
    }

    rat[p].lastPoll = t_ms_cpy;
  }
}

int main(){

	init_TX0();	// Hardware Serial Tx

	init_spi();  //Analog to Digital Converters

  init_clk(); // Timer0 @100kHz
  init_timer(); // Timer1 @1kHz
	init_panel();	// Knobs/Switches


  puts("\nrat4 welcome\n");
  while(1){
    for(uint8_t i=0; i<NUM_RAT; i++){
      check_butt(i);

      check_rat(i);

      if( rat[i].cntr >= WAVE_TABLE_SIZE){
        rat[i].cntr = 0;
      }

      uint16_t step_i = (uint16_t)( rat[i].cntr + rat[i].zero );
      step_i %= WAVE_TABLE_SIZE;

      if(step_i != rat[i].step){ //new step
        // estimates the delay introduced by checking the pannel
        uint16_t _dly = (step_i - rat[i].step)-(uint16_t)(rat[i].incr);
        if( (_dly > 1) & (_dly!=(-384)) ){ //W! often = 1
          /* printf("\ni_%i_d%i",i,_dly); */
        }
        rat[i].step = step_i;
        /* printf("\ni_%i_w%i",i,rat[i].wave); */

        /* rat[0].wave = 1; */
        /* rat[2].wave = 5; */
        uint16_t data_i = 0;
        switch(rat[i].wave){
          case 0:
            data_i = off(); break;
          case 1:
            data_i = hold(get_Cnst(i)); break;
            //WARNING: check if pot doesn't slow things down here
          case 2:
            data_i = saw_up(step_i); break;
          case 3:
            data_i = saw_dwn(step_i); break;
          case 4:
            data_i = square(step_i); break;
          case 5:
            data_i = squircle(step_i); break;
          case 6:
            data_i = triangle(step_i); break;
          case 7:
            data_i = sine(step_i); break;
          case 8: case 9:
            if(step_i < (uint16_t)(1*rat[i].incr) ){ //sample
              rat[i].sample = (i==c ? noise() :  data_h );
              /* printf("\ni_%i_s_%i_smp%i",i,step_i,rat[i].sample); */
            }
            if(!(rat[i].wave % 2)){ //case 8
              if(i==c){
                data_i = hold(rat[i].sample); //sample and hold
              }else{
                data_i = hump(step_i, rat[i].sample); //sample and hump
              }
            }else{ //case 9
              data_i = glide(step_i, rat[i].sample); //sample and glide
            }
            break;
          default:
            printf("wave:%i",rat[i].wave);
            data_i = 666; break;

        }
        send_spi_int(data_i, RAT_CS_PIN[i], RAT_SPI_CH[i]);
        if (i==c){data_h = data_i;} // take conductor's sample
      }

    }//for
  }//while(1)
}//main()
