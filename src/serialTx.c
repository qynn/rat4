/***************************************************************
* project : rat4 (quad CV LFO)
* target  : Atmega328
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Serial UA(R)T (TX0) for Atmega328 (16MHz)
*
****************************************************************/
#include "serialTx.h"

#ifndef F_CPU
  #define F_CPU 16000000UL
#endif

#define BAUD 115200
// BaudRate Expression for double speed Mode :
// UBBR = FCPU/8BAUD -1 +0.5 (rounded)
//#define UBRRV (( (F_CPU) + 4UL * (BAUD) ) / (8UL * (BAUD) ) - 1UL)

// BaudRate Expression for normal asynchronous Mode :
// UBBR = FCPU/16BAUD -1 +0.5 (rounded)
#define UBRR0V (( (F_CPU) + 8UL * (BAUD) ) / (16UL * (BAUD) ) - 1UL)
/*alternately check  #include <util/setbaud.h> */

static FILE stx_stream = FDEV_SETUP_STREAM(stx_putchar, NULL, _FDEV_SETUP_WRITE);

void init_TX0(void) {
    UBRR0H = (UBRR0V>>8);     			// UBBRV MSB
    UBRR0L = (UBRR0V& 0xff); 			// UBBRV LSB
    //UCSR0A |= (1<<U2X0); 			// using 2X speed
    UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00); 	// 8N1 (sync) mode
    UCSR0B |= (1<<TXEN0);   			// enable TX0

    stdout = &stx_stream;
}


/* FILE uart_input = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ); */
/* char uart_getchar(FILE *stream) { */
/*   loop_until_bit_is_set(UCSR0A, RXC0); /\* Wait until data exists. *\/ */
/*   return UDR0; */
/* } */


int stx_putchar(char c, FILE *stream){
  if(c == '\n'){
    stx_byte('\r');
    /* stx_putchar('\r',stream); //automatically adding carriage return w/ eol */
  }
  stx_byte(c);
  return 1;
}



void stx_byte(uint8_t b) {
    wait_for_flag(UCSR0A, UDRE0); /* Wait until data register is empty. */
    UDR0 = b;
} /*alternately wait until transmission is ready: wait_for_flag(UCSR1A,TXC1)*/


void stx_int(int a){// int16_t
    uint8_t lgth=INT_BUFF_LGTH; //including nul character ('\0')
    char buf[lgth];
    char *str=&buf[lgth-1]; //now points to last character
    *str ='\0'; //nul character to terminate string
    char sgn = ' ';
    if (a<0){
      a=-a;
      sgn = '-';
    }
    do{
        char c = a % 10;
        *(--str)= c + '0'; //ascii digits start at '0'=48;
        a/=10;
    }while(a);
    *(--str) = sgn;
    stx_str(str);
}

void stx_str(char *s){
    uint8_t i= 0; //counter

    stx_byte('\0'); //send dummy byte to avoid errors on first character
    while ( (s[i] != '\0') & (i < STR_MAX_LGTH) ) //end of string
    {
        stx_byte(s[i]);
        i++;
    }
}
