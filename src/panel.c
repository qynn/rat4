/*******************************************************************
* project : qr347 (8-step Midi Sequencer)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Functions for reading Pot/Switch values on front panel
*
********************************************************************/

#include "inc/panel.h"

/******* Rat Pots ************/

/* @param i : rat id number [0-NUM_RAT-1] */
/* @param X : clock multiplier */
uint16_t get_Ntck(uint8_t i, uint8_t X){
    set_MPX(CH_i[i]); // set ADC channel
    read_ADC_8bit(); // dummy read
    uint8_t a = read_ADC_8bit(); //0-255
    /* uint16_t _ntck = (uint16_t)(1.0*X*PPQN/96.0*get_Ntck_96PPQN(i)); */
    if(PPQN==96){
      return (uint16_t)(X*NTCK_96PPQN[a]);
    }
    else{
      return (uint16_t)(X*(PPQN/96.0)*NTCK_96PPQN[a]);
    }
}
/* float reading between 0 and 1 w/ 10-bit resolution */
/* @param i : rat id number [0-NUM_RAT-1] */
/* float get_Zero(uint8_t i){ */
/*   set_MPX(CH_i[i]); // set ADC channel */
/*   uint16_t a = read_ADC_10bit(); //0-1023 */
/*   return WAVE_TABLE_SIZE*(a/1023.0); */
/* } */

/* using phase lookup table w/ 8-bit analog read*/
/* @param i : rat id number [0-NUM_RAT-1] */
float get_Zero(uint8_t i){
  set_MPX(CH_i[i]); // set ADC channel
  read_ADC_8bit(); // dummy read
  uint8_t a = read_ADC_8bit(); //0-255
    return WAVE_TABLE_SIZE*(PHASE_LOOKUP[a]/240.0);
}

/* reading between 0 and 4095 w/ 10-bit resolution */
/* @param i : rat id number [0-NUM_RAT-1] */
uint16_t get_Cnst(uint8_t i){
  set_MPX(CH_i[i]); // set ADC channel
  uint16_t a = read_ADC_10bit(); //0-255
  return (uint16_t)(WAVE_AMP_MAX*(a/1023.0));
  /* return a; */
}

/******* Push Buttons  ************/

void init_pb(void){ // set PB pins as input
	DDR_PB &= ~(1<<PB_1) & ~(1<<PB_2) & ~(1<<PB_3) & ~(1<<PB_4);
}

uint8_t check_pb(uint8_t i){
	return !(PRT_PB & (1<<PB_i[i])); //Pin to GND
}

void init_sw(void){
 /* Warning: setting up PB2 (SS) as input will lock the MCU */
  DDR_SW &= ~(1<<SW_W); // toggle switch as input
  DDR_LED |= (1<<PIN_LED); // LEDcator as output
  PRT_LED &= ~(1<<PIN_LED); //turn LED on at startup
}

uint8_t check_sw(void){ // check Mode Switch
	return ( !(PRT_SW & (1<<SW_W)) ? 1 : 0);  //Pin to GND
}

/******* Wave Select *********/
uint8_t get_wave(void){
	set_MPX(CH_W);
	uint8_t a = read_ADC_8bit(); //0-255
	a = (a>>5); //0-7
  uint8_t b = check_sw();
	return b + SLIDER_TABLE[a];
  }

/* test Alt+j
 * this is
 * a comment
 * obviously
 */

void init_panel(void){
  init_ADC();
  init_pb();
  init_sw();

}
