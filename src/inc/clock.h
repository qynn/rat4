/*****************************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328 (20MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
******************************************************************/
#ifndef CLOCK_H
#define CLOCK_H

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>

/* -----INTERNAL CLOCK-------*/
#define PPQN 48 // Pulses per Quarter Note (Max for Beatstep Pro)

#define TEMPO_MIN	5 // BPM
#define TEMPO_MAX	360 // BPM
/*NB: a quarter note has a duration of exactly one beat */
/* so 1BPM = 1/60 quarter-note per second */
static const uint32_t TCLK_MIN = (60000000/(TEMPO_MAX*PPQN)); // us
static const uint32_t TCLK_MAX = (60000000/(TEMPO_MIN*PPQN)); // us
/* Warning : there is crosstalk between P1 and P0, Clock potentiometer */
/*   does not actually reach 0 so min tempo will not be effective */
/* e.g. @30BPM; 48PPQN >> TCLK_MAX = 41666us, but only measured 33756 (unstable)*/

#define INT_XCLK 2// default Clock Multiplier in Internal Mode

static const int EXT_CLK_TIMEOUT = 5000; // timeout counter max
/* The effective timeout in us will be equal to EXT_CLK_TIMEOUT times TCLK_MAX */
/* e.g. @30BPM; PPQN=48 >> TCLK_MAX = 41666us; */
/* to get a 500ms timeout one must set EXT_CLK_TIMEOUT to 12 */

#define NUM_XCLK 5
/* check those numbers; there was cross talk between P0 and P1 */
static const uint8_t XCLK_TABLE[2][NUM_XCLK] = {{163, 229, 242, 245 ,250},
                                                {32, 16, 8, 4, 2}};
void init_clk(void);

void init_timer(void);

uint32_t get_Tclk(void);

uint8_t get_Xclk(void);

void cntr_incr(void);

void cntr_zero(void);

#endif /*CLOCK_H*/
