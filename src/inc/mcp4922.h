/***********************************************************
* project : rat4 (quad CV LFO)
* target  : Atmega328
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
************************************************************/

#ifndef MCP4922_H
#define MCP4922_H

/* #include <stdlib.h> */
/* #include <stdint.h> */
/* #include <stdio.h> */
#include <avr/io.h>

#define SPI_PRT PORTB
#define SPI_DDR DDRB

#define SCK_PIN 5
#define MOSI_PIN 3
#define MISO_PIN 4
#define SS_PIN 2

#define CS1_PIN 0
#define CS2_PIN 1
#define SPI_CHA 0
#define SPI_CHB 1

void init_spi(void);

void send_spi_byte(uint8_t b);

void send_spi_int(uint16_t data, uint8_t csPin, uint8_t channel);

#endif /* MCP4922_H */
