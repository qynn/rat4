/********************************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Functions for reading Pot/Switch values on front panel
*
********************************************************************/

#ifndef PANEL_H
#define PANEL_H

#include <stdint.h>
#include "clock.h"
#include "adcmux.h"
#include "ntckTable.h"
#include "phaseTable.h"
#include "waves.h"

#define PANEL_POLL_DELAY 100 //ms

// Potentiometers
#define CH_C 0  // (C) Clock
#define CH_1 1	// (1) Rat1
#define CH_2 2	// (2) Rat2
#define CH_3 3	// (3) Rat3
#define CH_4 4	// (4) Rat4
#define CH_W 5	// (W) Wave
static const uint8_t CH_i[4]={CH_1,CH_2,CH_3,CH_4};

// Push-Buttons
#define PRT_PB PIND
#define DDR_PB DDRD
#define PB_1 4  // Start1 (S1)
#define PB_2 5	// Start2 (S2)
#define PB_3 6	// Start3 (S3)
#define PB_4 7	// Start4 (S4)
static const uint8_t PB_i[4]={PB_1,PB_2,PB_3,PB_4};
#define PUSH_LONG 800 //loop iterations

// Wave Bank Switch
#define PRT_SW PINB
#define DDR_SW DDRB
#define SW_W 4 // Wave Bank (S5)
/*
  Warning! S5=PB4 is shared w/ MISO pin (input only)
  avrdude cannot flash if Mode switch is pulling S5 to GND
  must keep mode switch lever up before programming
 */
#define PRT_LED PORTD
#define DDR_LED DDRD
#define PIN_LED 0 // Mode LEDcator
/* Warning : had issues when using PB2 = SS */

// SLIDER POT
#define SLIDER_RES 8
static const uint8_t SLIDER_TABLE[SLIDER_RES] = {
                            0, 2, 2, 4, 4, 6, 6, 8 };

uint16_t get_Ntck(uint8_t i, uint8_t X);

float get_Zero(uint8_t i);

uint16_t get_Cnst(uint8_t i);

void init_panel(void);

void init_pb(void);

uint8_t check_pb(uint8_t i);

void init_sw(void);

uint8_t check_sw(void);

uint8_t get_wave(void);

#endif /* PANEL_H */
