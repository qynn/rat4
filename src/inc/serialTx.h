/**********************************************
* file    : serialTx.h
* project : rat4
* target  : Atmega328
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Serial UA(R)T (TX0) for Atmega328 (16MHz)
*
************************************************/

#ifndef SERIALTX_H
#define SERIALTX_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>

#define STR_MAX_LGTH	20   // Maximum length for Strings
#define INT_BUFF_LGTH	7   // 1 sign + 5digits + nul character

/*replacement of loop_until_bit_is_set()*/
#define wait_for_flag(port, bitnum) \
while ( ! ( (port) & (1 << (bitnum)) ) ) ;

void init_TX0(void);

void stx_byte(uint8_t b);

void stx_int(int a);

void stx_str(char *s);

int stx_putchar(char c, FILE *stream);

#endif /* SERIALTX_H */
