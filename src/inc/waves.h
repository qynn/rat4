/****************************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Functions to generate/update LFO waves (see waves.c)
*
****************************************************************/

#ifndef WAVES_H
#define WAVES_H

#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include "sinTable.h"

#define WAVE_AMP_MAX 4095

uint16_t map_up(uint16_t i, uint16_t imax);

uint16_t map_dwn(uint16_t i, uint16_t imax);

uint16_t sine(uint16_t i);

uint16_t square(uint16_t i);

uint16_t squircle(uint16_t i);

uint16_t saw_up(uint16_t i);

uint16_t saw_dwn(uint16_t i);

uint16_t triangle(uint16_t i);

uint16_t noise(void);

uint16_t off(void);

uint16_t hold(uint16_t sample);

uint16_t hump(uint16_t i, uint16_t sample);

uint16_t glide(uint16_t i, uint16_t sample);

#endif /*WAVES_H*/
