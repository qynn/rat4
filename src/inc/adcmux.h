/******************************************************************
* file    : adcmux.h (header for adcmux.c)
* project : qr347 (8-step Midi Sequencer)
* target  : ATmega328P (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Minimal AnalogRead() for ATmega328P
*
*******************************************************************/

#ifndef ADCMUX_H
#define ADCMUX_H

void init_ADC(void); //init ADC: single-ended input, Vref=VCC, left-shifted result

void set_MPX(uint8_t ch); //select ADC channel (0 to 7)

uint8_t read_ADC_8bit(void); // start conversion and read MSB

uint16_t read_ADC_10bit(void); //start conversion and read (MSB + LSB)

#endif /* ADCMUX_H */
