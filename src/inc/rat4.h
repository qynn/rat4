/**************************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328 (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
***************************************************************/
#ifndef RAT4_H
#define RAT4_H

#define DBG_PRT 0
#define F_CPU 16000000UL

#include "waves.h"
#include "clock.h"
#include "panel.h"
#include "serialTx.h"
#include "mcp4922.h"

#define NUM_RAT 4

struct Rat{
  uint8_t wave; // selected wave id
  uint16_t push; // push counter
  uint16_t sample; // for sample&hold
  volatile float cntr; // current step
  uint16_t step; // last step
  volatile float incr; // step increment = TABLE_SIZE/Ntck
  volatile float zero; // step offset
  uint16_t Ntck; //nb of ticks per Wave Period = Q*x*PPQN
  uint16_t lastPoll; // last ms when pot polled
};

/* Mode switch */
#define RATE_MODE 0
#define PHASE_MODE 1

/* mcp4922 addresses */
static const uint8_t RAT_CS_PIN[NUM_RAT] = {CS1_PIN, CS1_PIN, CS2_PIN, CS2_PIN};
static const uint8_t RAT_SPI_CH[NUM_RAT] = {SPI_CHA, SPI_CHB, SPI_CHA, SPI_CHB};

#endif /*RAT4_H*/
