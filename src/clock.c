/****************************************************
* project : rat4 (quad CV LFO)
* target  : ATmega328 (20MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* CLOCK generation
*
****************************************************/

#include "inc/clock.h"
#include "inc/panel.h"
#include "inc/rat4.h"

#ifndef F_CPU
  #define F_CPU 16000000UL
#endif

/* external clock */
extern volatile uint8_t extClkOn;    // 1 if CV clock is active
volatile int extClkTimeout; // increments when CV clock becomes inactive


/*general purpose timer*/
volatile uint16_t t_ms=0;   // time in milliseconds

/* internal clock */
volatile uint32_t t_us=0;   // time in microseconds
extern volatile uint32_t Tclk;  //Pulse length in microseconds

extern struct Rat rat[4];

void init_clk(void){ /* Set up TIMER0 at fHz=100kHz (CTC mode) */
	/*  F_CPU = 16MHz ; fHz=100kHz ; Pre = 1
	 * 	OCR0A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(100*10^3*1) -1] = 159 <256!!
	 */
  cli(); 						// disable global interrupts

  TCCR0A = 0;					// set entire TCCR0A register to 0
	TCCR0B = 0;					// same for TCCR0B
	TCNT0  = 0;					// initialize counter value to 0
	OCR0A = 159;  				// set Compare Register (see comment above)
	TCCR0A |= (1 << WGM01);		// clear timer on compare match (CTC mode on)
	TCCR0B |= (1 << CS00);   	// no prescaling
	TIMSK0 |= (1 << OCIE0A); 	// enable interrupts on TIMER0 (COMPA)

  //set external interrupt on CLOCK=INT0=PD2  & SYNC=INT1=PD3
	DDRD &= (~(1<<2)) &  (~(1<<3));  	// PD2 & PD3 as inputs
  EICRA |= (1<<ISC01); 	// set INT0 as falling edge
  EIMSK |= (1<<INT0); 						//turn on INT0='D2'
  EICRA |= (1<<ISC11); 	// set INT1 as falling edge
  EIMSK |= (1<<INT1); 						//turn on INT1='D3'

  sei();						// enable global interrupts
}

void init_timer(void){ /* Set up TIMER1(16-bit) at fHz=1kHz (CTC mode) */
	/*  F_CPU = 16MHz ; fHz=1000Hz ; Pre = 1
	 * 	OCR1A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(1*10^3*1) -1] = 15999 < 65535!!
	 */
  cli(); 						// disable global interrupts

  TCCR1A = 0;					// set entire TCCR1A register to 0
	TCCR1B = 0;					// same for TCCR1B
	TCNT1  = 0;					// initialize counter value to 0
	OCR1A = 15999;  				// set Compare Register (see comment above)
	TCCR1B |= (1 << WGM12)|(1 << CS10);  // CTC1 mode on + no prescaling
	TIMSK1 |= (1 << OCIE1A); 	// enable interrupts on TIMER1 (COMPA)

  sei();						// enable global interrupts
}

ISR(TIMER1_COMPA_vect){ //TIMER1 @ 1kHz (dt=1ms)
	/* if(++t_ms==10000){t_ms=0;} // (ms) */
  t_ms++; // (ms)
}

ISR(TIMER0_COMPA_vect){ //TIMER0 @ 100kHz (dt=0.01ms=10us)
	t_us+=10; // (us)

  if(extClkOn & (t_us>TCLK_MAX) ){
    extClkTimeout++;
    if(extClkTimeout==EXT_CLK_TIMEOUT){
      extClkOn=0;
      /* extClkTimeout=0; */
    }
  }
	else if(!extClkOn & (t_us>Tclk)){ // internal clock tick
    cntr_incr();
		/* t_us=0; */
		t_us-=Tclk;
	}
}

/******* Internal Clock Pulse Length*********/
uint32_t get_Tclk(void){
  set_MPX(CH_C);
  read_ADC_10bit(); //dummy read
  uint16_t pot=read_ADC_10bit();

  uint32_t tclk=(uint32_t)(TCLK_MAX-1.0*pot*(TCLK_MAX-TCLK_MIN)/1023.0);
  return tclk;
}

/******* Clock Multiplier*********/
uint8_t get_Xclk(void){
  uint8_t ret =1;
  set_MPX(CH_C);
  read_ADC_8bit(); //dummy read
  uint8_t pot = read_ADC_8bit();
  for (int i=0; i< NUM_XCLK; i++){
    if( pot < XCLK_TABLE[0][i] ){
      ret = XCLK_TABLE[1][i];
      break;
    }
  }
  return ret;
}

void cntr_incr(void){
  for(uint8_t i=0; i<NUM_RAT; i++){
    rat[i].cntr += rat[i].incr;
  }
}

void cntr_zero(void){
  for(uint8_t i=0; i<NUM_RAT; i++){
    rat[i].cntr = 0;
  }
}

/* Warning : C and S have been inverted on layout
 */

ISR(INT0_vect){ //falling edge INT0=CLOCK
  extClkOn=1;
  cntr_incr();
  t_us=0;
  extClkTimeout=0;
}

ISR(INT1_vect){ //falling edge INT1=SYNC
  t_us=0;
  cntr_zero();
}
