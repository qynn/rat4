- [rat4 Quad CV LFO](#sec-1)
  - [testing Latex](#sec-1-1)
    - [test](#sec-1-1-1)
- [Master Clock](#sec-2)
- [Dual Clock mode](#sec-3)
  - [External CV clock](#sec-3-1)
  - [Internal Clock Mode](#sec-3-2)
- [Rate Mode](#sec-4)
    - [Clock settings @ Xclk=32 (aka x4)](#sec-4-0-1)
    - [Clock settings @ Xclk=8 (aka x1)](#sec-4-0-2)
    - [Clock settings @ Xclk=1 (aka /8)](#sec-4-0-3)
- [Modes](#sec-5)
- [press Ctrl-Enter to add new heading](#sec-6)
  - [test](#sec-6-1)

# rat4 Quad CV LFO<a id="sec-1"></a>

## testing Latex<a id="sec-1-1"></a>

   \begin{equation}                        % arbitrary environments,
   x=\sqrt{b}                              % even tables, figures
   \end{equation}                          % etc


\begin{equation}
  \label{simple_equation}
  \alpha = \sqrt{ \beta }
\end{equation}

If \(a^2=b\) and \( b=2 \), then the solution must be either \[ a=+\sqrt{2} \] or \[ a=-\sqrt{2} \]. testing latex

\[ e^{i\pi} = -1 \]

\[ \int_0^\infty e^{-x^2} dx = \frac{\sqrt{\pi}}{2} \]

### test<a id="sec-1-1-1"></a>

test

1.  line1
2.  

3.  line2

1.  test2

    based on Atmega328P

# Master Clock<a id="sec-2"></a>

-   1 CV input (0;5V)
-   1 Potentiometer (if CV is nil)
-   Midi input (optional)

# Dual Clock mode<a id="sec-3"></a>

The LFO can either be synced to an external CV clock (CLK input) or run in free mode with an internal clock

The clock system enters external mode as soon as a falling edge is detected on the CLK input. It automatically falls back to internal mode when no falling edge has been detected after a given timeout period. The \`EXT<sub>CLK</sub><sub>TIMEOUT</sub>\` value is set to 254ms by default. Note this is slightly longer than the period of a 1/4th square wave (1PPQN) running at 60BPM.

## External CV clock<a id="sec-3-1"></a>

The LFO module was initially intended to be synced with a **Arturia BeatStep Pro** whose CV output clock can be set to either 24 or 48 PPQN. One design constraint was to reach a minimum wave period of a thirty-second-note (1/32) when set at its maximum rate. In the worst case scenario (24 PPQN), this leaves us with 1/32\*4\*24 = 3 pulses per period, which is only 1 pulse more than the absolute minimum for a outputting a square wave.

In External Clock Mode, the Master Clock Potentiometer (P0) is used as a clock multiplier, offering six different settings : Xclk = 1; x2; x4; x8; x16; x32;

## Internal Clock Mode<a id="sec-3-2"></a>

In Internal Clock Mode, the Master Clock Potentiometer (P0) is used to control the tempo. By default the tempo range extends from 30 to 160BPM, and the internal Clock Multiplier is set to \`INT<sub>CLK</sub> = 8\`.

A convenient labelling for P0 might be to call \`1\` the position were the internal and external clock multipliers match and deducting the others accordingly as shown in the table bellow;

| External Xclk          | 1  | 2  | 4  | 8  | 16 | 32 |
| INT<sub>XCLK</sub> = 8 | /8 | /4 | /2 | x1 | x2 | x4 |

An 8-bit lookup table has been created to map P0's output to the number of clock ticks (pulses) contained in a single wave period. Steps have been created around particular values as shown in the following graph.

\![Ntck<sub>96PPQN</sub>](doc/Ntck<sub>96PPQN.svg</sub>)

1.  For a given clock multiplier (affects all waves)

what's the ratio between tne max and min rate? this will impact the knob resolution: 1/32 X 16 = 1/2

1.  at the absolute lowest rate how many bars should

the wave last for? how about 16? then 32 multiplier

# Rate Mode<a id="sec-4"></a>

### Clock settings @ Xclk=32 (aka x4)<a id="sec-4-0-1"></a>

| Symbol    | ratx       | Rate       | wave/bar            | QN/wave             | N. ticks   |
|           |            | @x4        |                     | @4x4                | @96ppqn    |
| &#x2014;: | :&#x2014;: | :&#x2014;: | :-&#x2013;&#x2014;: | :-&#x2013;&#x2014;: | :&#x2014;: |
| -4        | 2          | 1/2        | 2                   | 2                   | 192        |
| -3        | 4/3        | 1/2T       | 3                   | 4/3                 | 128        |
| -2        | 1          | 1/4        | 4                   | 1                   | 96         |
| -1        | 2/3        | 1/4T       | 6                   | 2/3                 | 64         |
| 0         | 1/2        | 1/8        | 8                   | 1/2                 | 48         |
| +1        | 1/3        | 1/8T       | 12                  | 1/3                 | 32         |
| +2        | 1/4        | 1/16       | 16                  | 1/4                 | 24         |
| +3        | 1/6        | 1/16T      | 24                  | 1/6                 | 16         |
| +4        | 1/8        | 1/32       | 32                  | 1/8                 | 12         |

### Clock settings @ Xclk=8 (aka x1)<a id="sec-4-0-2"></a>

| Symbol    | ratx       | Rate       | wave/bar           | QN/wave             | N. ticks   |
|           |            | @x1        |                    | @4x4                | @96ppqn    |
| &#x2014;: | -&#x2014;: | :&#x2014;: | :&#x2013;&#x2014;: | :-&#x2013;&#x2014;: | :&#x2014;: |
| -4        | 2          | 2          | 2                  | 2                   | 192        |
| -3        | 4/3        | 4/3        | 3                  | 4/3                 | 128        |
| -2        | 1          | 1          | 4                  | 1                   | 96         |
| -1        | 2/3        | 2/3        | 6                  | 2/3                 | 64         |
| 0         | 1/2        | 1/2        | 8                  | 1/2                 | 48         |
| +1        | 1/3        | 1/2T       | 12                 | 1/3                 | 32         |
| +2        | 1/4        | 1/4        | 16                 | 1/4                 | 24         |
| +3        | 1/6        | 1/4T       | 24                 | 1/6                 | 16         |
| +4        | 1/8        | 1/8        | 32                 | 1/8                 | 12         |

### Clock settings @ Xclk=1 (aka /8)<a id="sec-4-0-3"></a>

| Symbol    | ratx       | Rate     | wave/bar            | QN/wave             | N. ticks   |
|           |            | @/8      |                     | @4x4                | @96ppqn    |
| &#x2014;: | -&#x2014;: | :------- | :-&#x2013;&#x2014;: | :-&#x2013;&#x2014;: | :&#x2014;: |
| -4        | 2          | 16       | 1/16                | 64                  | 6144       |
| -3        | 4/3        | 16T      | 3/32                | 48                  | 4096       |
| -2        | 1          | 8        | 1/8                 | 32                  | 3072       |
| -1        | 2/3        | 8T       | 3/16                | 24                  | 2048       |
| 0         | 1/2        | 4        | 1/4                 | 16                  | 1536       |
| +1        | 1/3        | 4T       | 3/8                 | 32/3                | 1024       |
| +2        | 1/4        | 2        | 1/2                 | 8                   | 768        |
| +3        | 1/6        | 2T       | 3/4                 | 16/3                | 512        |
| +4        | 1/8        | 1        | 1                   | 4                   | 384        |

# Modes<a id="sec-5"></a>

-   1 mode Switch (1:Rate 2:Phase)
-   1 parameter Pot per LFO

In Rate mode each LFO has a rate that is a mutliple of the Master clock In Phase mode, all 4 LFOs share the same rate but can be phase-shifted

Outputs

-   1 LED indicator per LFO
-   1 CV output (-5;+5V) per LFO

Two SPI dual-DACs (MCP4922) are used.

Shape

-   1 Waveshape selector
-   1 push button per LFO

First select the waveshape; then push the LFO button to assign it

Tri; Saw; Sine; Square; Sinetooth; Noise; S&H;

Layout

Maximum PCB dimensions: 110mm x 47mm fPannel : 128,5 x (15mm + N x 5.08mm)

\`\`\` code block is this going to change

\`\`\`

Errors

1.  FP: BJT's NPN transistors use the E-B-C pinout

that is E=1; B=2; C=3 for some reason in kicad i used B=1; C=2; E=3; thinking it would be standard 2: MB : SHDN must be HIGH , not LOW on MCP4922, otherwise both channels are shut down !!! <span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline"><span class="underline">\_\_</span></span></span></span></span></span></span></span>

RC filter at tempo 160BPM there are 160/60 = 2,67 beats (QN) per second; with a 4x4 signature there are 4 beats (QN) per bar 1 bar lasts then 1.5 seconds at 160BPM at R=1/32 there are 64 raising edges per bar in a square wave its half period is then of 1500/64 ~ 23ms to obtain a settling time of 3Tau = 30%\*23ms >> Tau = 2.3ms with R=220R >> C ~ 2200us/220 = 10uF

NB: 96PPQN @ 160BPM >> 96\*160/60 = 256 Hz >> dt=3.9ms would be nice to have 3Tau = 3.9ms >> Tau = 1.3ms >> C ~ 6uF

NB: i've used 220R resistor and 10uF (should expect Tau = 2.2ms) at max speed : Tclk = 3906 us and N = 12 >> 12x3.9/2 = 23.4ms measured about 4.8 divisions at 5ms/div >> 24ms for 1/2 a wave : OK problem is the RC filtered output seems to have reached only 80% of its max meaning 3Tau>24ms; Tau>8ms, almost 4 times longer than expected

using C=0.1uF was too fast: 3Tau~1ms >> Tau~0.33ms instead of the expected 220x0.1 = 0.022ms there is about an order 10 between the calculated value and the one read. so to acutally get 3Tau~3.9ms; lets say RC~3.9\*4=15.6ms >> C~0.7uF. 1uF could make sense here

# press Ctrl-Enter to add new heading<a id="sec-6"></a>

use Alt+h/l to shift indentation

1.  

2.  test
3.  test2
4.  wtf
5.  slash bullet
6.  wtf

## test<a id="sec-6-1"></a>
