% Created 2019-12-07 Sat 13:51
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{qusw}
\date{\today}
\title{}
\hypersetup{
 pdfauthor={qusw},
 pdftitle={},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.2.4)}, 
 pdflang={English}}
\begin{document}

\tableofcontents

\section{rat4 Quad CV LFO}
\label{sec:orgadfe5d7}

based on Atmega328P

\section{Master Clock}
\label{sec:org84be0cc}
\begin{itemize}
\item 1 CV input (0;5V)
\item 1 Potentiometer (if CV is nil)
\item Midi input (optional)
\end{itemize}

\section{Dual Clock mode}
\label{sec:orgfd1ae30}
rat4 can either be synced to an external CV clock (CLK input) or run in
free mode with an internal clock

The clock system enters external mode as soon as a falling edge is detected on the CLK input. It automatically falls back to internal mode when no falling edge has been detected after a given timeout period. The \(EXT\_CLK\_TIMEOUT\) value is set to 254ms by default. Note this is slightly longer than the period of a 1/4th square wave (1PPQN) running at 60BPM.

\subsection{External CV clock}
\label{sec:orgbdc46ee}
rat4 was initially intended to be synced with a \textbf{Arturia BeatStep Pro} whose CV output clock can be set to either 24 or 48 PPQN. One design constraint was to reach a minimum wave period of a thirty-second-note (1/32) when set at its maximum rate. In the worst case scenario (24 PPQN), this leaves us with 1/32*4*24 = 3 pulses per period, which is only 1 pulse more than the absolute minimum for a outputting a square wave.

In External Clock Mode, the Master Clock Potentiometer (P0) is used as a \textbf{clock
divider}, offering six different settings: `Xclk = 32; 16; 8; 4; 2; 1;`
32 being the slowest mode (32 times slower as the fastest, unity clock)
Note : More specifically, `Xclk` is the multiplier applied to the number of clock ticks contained in one wave period, therefore the higher it is the slower the wave frequency becomes.

\subsection{Internal Clock Mode}
\label{sec:org191942c}
In Internal Clock Mode, the Master Clock Potentiometer (P0) is used to control the tempo. By default the tempo range extends from 30 to 160BPM, and the internal Clock Multiplier is set to `INT\textsubscript{CLK} = 8`.

A convenient labelling for P0 might be to call `1` the position were the external clock divider matches the internal mode setting and deducting the other labels accordingly as shown in the table bellow;

\begin{center}
\begin{tabular}{l|rrrrrr|}
External Xclk & 32 & 16 & 8 & 4 & 2 & 1\\
\hline
Int. Xclk=8 & /4 & /2 & x1 & x2 & x4 & x8\\
\end{tabular}
\end{center}


An 8-bit lookup table has been created to map P0's output to the number of clock ticks (pulses) contained in a single wave period. Steps have been created around particular values as shown in the figure below.

![Ntck\textsubscript{96PPQN}](doc/Ntck\textsubscript{96PPQN.svg})

[[\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/Ntck_96PPQN.png}
\caption{\label{fig:Ntck_96PPQN}
Number of clock Ticks per wave Period}
\end{figure}]]

\section{Rate vs Phase Modes}
\label{sec:orge7a0cb7}

Two different LFO modes are currently implemented : `Rate` and `Phase`. To switch modes, long press (>1 second) on any of the sync push buttons S1..4. The mode LED indicator will be ON when currently in Phase mode At startup the LFO enters Rate Mode automatically and the \textbf{conductor} LFO (see Waveforms section for more details) defaults to `rat1`.

\subsection{Rate Mode}
\label{sec:org87f17b8}

In rate mode, all four LFOs have different rates which can be adjusted using the P1..4 knobs. The tables below summarize the rates available for three particular clock settings:

\subsubsection{Clock settings @ Xclk=1 (aka x8)}
\label{sec:org2d67116}
\begin{center}
\begin{tabular}{rllrlr}
Symbol & ratx & Rate & wave/bar & QN/wave & N. ticks\\
 &  & @x8 &  & @4x4 & @96ppqn\\
---: & :---: & :---: & :------: & :------: & :---:\\
-4 & 4 & 1/2 & 2 & 2 & 192\\
-3 & 8/3 & 1/2T & 3 & 4/3 & 128\\
-2 & 2 & 1/4 & 4 & 1 & 96\\
-1 & 4/3 & 1/4T & 6 & 2/3 & 64\\
0 & 1 & 1/8 & 8 & 1/2 & 48\\
+1 & 1/6 & 1/8T & 12 & 1/3 & 32\\
+2 & 1/2 & 1/16 & 16 & 1/4 & 24\\
+3 & 1/3 & 1/16T & 24 & 1/6 & 16\\
+4 & 1/4 & 1/32 & 32 & 1/8 & 12\\
\end{tabular}
\end{center}

\subsubsection{Clock settings @ Xclk=8 (aka x1)}
\label{sec:orga9378d2}
\begin{center}
\begin{tabular}{rllllr}
Symbol & ratx & Rate & wave/bar & QN/wave & N. ticks\\
 &  & @x1 &  & @4x4 & @96ppqn\\
---: & ----: & :---: & :-----: & :------: & :---:\\
-4 & 4 & 4 & 1/4 & 16 & 1536\\
-3 & 8/3 & 4T & 3/8 & 32/3 & 1024\\
-2 & 2 & 2 & 1/2 & 8 & 768\\
-1 & 4/3 & 2T & 3/4 & 16/3 & 512\\
0 & 1 & 1 & 1 & 4 & 384\\
+1 & 1/6 & 1T & 3/2 & 8/3 & 256\\
+2 & 1/2 & 1/2 & 2 & 2 & 192\\
+3 & 1/3 & 1/2T & 3 & 4/3 & 128\\
+4 & 1/4 & 1/4 & 4 & 1 & 96\\
\end{tabular}
\end{center}

\subsubsection{Clock settings @ Xclk=1 (aka /8)}
\label{sec:orgc3b7fcb}
\begin{center}
\begin{tabular}{rlllrr}
Symbol & ratx & Rate & wave/bar & QN/wave & N. ticks\\
 &  & @/8 &  & @4x4 & @96ppqn\\
---: & ----: & :------- & :------: & :------: & :---:\\
-4 & 4 & 16 & 1/16 & 64 & 6144\\
-3 & 8/3 & 16T & 3/32 & 48 & 4096\\
-2 & 2 & 8 & 1/8 & 32 & 3072\\
-1 & 4/3 & 8T & 3/16 & 24 & 2048\\
0 & 1 & 4 & 1/4 & 16 & 1536\\
+1 & 1/6 & 4T & 3/8 & 32/3 & 1024\\
+2 & 1/2 & 2 & 1/2 & 8 & 768\\
+3 & 1/3 & 2T & 3/4 & 16/3 & 512\\
+4 & 1/4 & 1 & 1 & 4 & 384\\
\end{tabular}
\end{center}

\subsection{Phase Mode}
\label{sec:org20ccc00}

In phase mode, all oscillators have the same rate but can be phase-shifted. To enter Phase Mode, long press (>1sec) on any of the four sync push buttons S1..4.

The common rate corresponds to the one of the \textbf{conductor} LFO whose sync push button Si has been pressed when entering the mode. The corresponding Pi knob can still be used to modify the rate of all four LFOs altogether.

Phase reference is set by the master LFO and all three other LFOs can be phase shifted from 0\textdegree{} (min) to 360\textdegree{} (max) by using their corresponding Pi knobs.

To leave Phase Mode, long press again on any of the four sync push buttons S1..4 to go into Rate Mode with the \textbf{conductor} LFO being the one whose button has been pushed. All LFOs will 

\section{Waveforms}
\label{sec:orgbb86f7a}

There are currently two banks of Waveforms implemented, each including 5 different shapes, as summarized in the Table below. The S5 Toggle switch is used to toggle Wave banks.

\begin{center}
\begin{tabular}{ll}
Bank A (up) & Bank B (down)\\
\hline
OFF (0V) & hold (adj. voltage)\\
saw up & saw down\\
square & squircle (TODO)\\
triangle & sinewave\\
sample $\backslash$& hold & sample $\backslash$& glide\\
\end{tabular}
\end{center}

\subsection{hold}
\label{sec:org401250a}

Hold is a constant voltage signal whose level is adjustable (-5V; 5V) using the Rate knob


\subsection{sample $\backslash$& hold}
\label{sec:org6fc5cc5}

For the conductor, a random noise is used to generate the samples.
Samples are held for half a period, then set back to 0V for the rest of the wave period, leading to a \textbf{random square} type.

For the three other LFOs, voltages are sampled directly from the conductor.   Depending on the conductor's wave and the rate ratio between the conductor and slave LFO, different behaviours can be obtained.

\subsection{sample $\backslash$& glide}
\label{sec:org29dbe7a}

The sample $\backslash$& glide is the same as the sample $\backslash$& hold wave except linear curves are used between consecutive samples, leading to a \textbf{random triangle} type.


\section{Draft}
\label{sec:org08a5e92}


\section{press Ctrl-Enter to add new heading}
\label{sec:orgd57ccf7}
\end{document}