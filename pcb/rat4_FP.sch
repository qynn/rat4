EESchema Schematic File Version 4
LIBS:rat4_FP-cache
EELAYER 26 0
EELAYER END
$Descr A 8500 11000 portrait
encoding utf-8
Sheet 1 1
Title "rat4 "
Date "2019-08-13"
Rev "v0.1"
Comp "./qusw"
Comment1 "based on ATmega328P"
Comment2 "Quad LFO"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:AudioJack2 J60
U 1 1 5D2B695E
P 6950 3350
F 0 "J60" H 6771 3425 50  0000 R CNN
F 1 "TS" H 6771 3334 50  0000 R CNN
F 2 "mod:CUI-MJ3507-bent" H 6950 3350 50  0001 C CNN
F 3 "~" H 6950 3350 50  0001 C CNN
	1    6950 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5D421BDC
P 1850 6550
F 0 "R3" H 1920 6596 50  0000 L CNN
F 1 "100K" V 1850 6450 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1780 6550 50  0001 C CNN
F 3 "~" H 1850 6550 50  0001 C CNN
	1    1850 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5D421BE9
P 1850 6750
F 0 "#PWR07" H 1850 6500 50  0001 C CNN
F 1 "GND" H 1855 6577 50  0000 C CNN
F 2 "" H 1850 6750 50  0001 C CNN
F 3 "" H 1850 6750 50  0001 C CNN
	1    1850 6750
	1    0    0    -1  
$EndComp
Text Label 2950 6150 0    60   ~ 0
INIT
Text Notes 2300 5300 0    60   ~ 0
INIT
$Comp
L Switch:SW_Push SW7
U 1 1 5D426201
P 1300 6100
F 0 "SW7" V 1250 6350 50  0000 R CNN
F 1 "PUSH" V 1400 6400 50  0000 R CNN
F 2 "mod:Push-PS1024" H 1300 6300 50  0001 C CNN
F 3 "" H 1300 6300 50  0001 C CNN
	1    1300 6100
	0    -1   1    0   
$EndComp
$Comp
L Device:R R66
U 1 1 5D4B6D97
P 5900 3350
F 0 "R66" V 5800 3300 50  0000 L CNN
F 1 "1K" V 5900 3300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5830 3350 50  0001 C CNN
F 3 "~" H 5900 3350 50  0001 C CNN
	1    5900 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 3350 6750 3350
Wire Wire Line
	6750 3650 6750 3450
$Comp
L Device:R R67
U 1 1 5D5DCED1
P 6500 3350
F 0 "R67" V 6400 3300 50  0000 L CNN
F 1 "220R" V 6500 3250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6430 3350 50  0001 C CNN
F 3 "~" H 6500 3350 50  0001 C CNN
	1    6500 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C62
U 1 1 5D5DD313
P 6200 3500
F 0 "C62" H 6315 3546 50  0000 L CNN
F 1 "0.1uF" H 6315 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 3350 50  0001 C CNN
F 3 "~" H 6200 3500 50  0001 C CNN
	1    6200 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3650 6750 3650
Wire Wire Line
	6200 3350 6350 3350
Wire Notes Line
	6050 3200 6050 3700
Wire Notes Line
	6050 3700 6700 3700
Wire Notes Line
	6700 3700 6700 3200
Wire Notes Line
	6700 3200 6050 3200
Text Notes 5950 3150 0    60   ~ 0
optional RC filter
Wire Wire Line
	5750 3350 5700 3350
Wire Wire Line
	5700 3000 5700 3350
Connection ~ 5700 3350
Wire Wire Line
	5700 3350 5550 3350
Connection ~ 6200 3350
Wire Wire Line
	6050 3350 6200 3350
Text Notes 6900 3150 0    60   ~ 0
CV LFO1
$Comp
L Connector:AudioJack2 J70
U 1 1 5D6C252D
P 6950 4850
F 0 "J70" H 6771 4925 50  0000 R CNN
F 1 "TS" H 6771 4834 50  0000 R CNN
F 2 "mod:CUI-MJ3507-bent" H 6950 4850 50  0001 C CNN
F 3 "~" H 6950 4850 50  0001 C CNN
	1    6950 4850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R76
U 1 1 5D6C258F
P 5900 4850
F 0 "R76" V 5800 4800 50  0000 L CNN
F 1 "1K" V 5900 4800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5830 4850 50  0001 C CNN
F 3 "~" H 5900 4850 50  0001 C CNN
	1    5900 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 4850 6750 4850
Wire Wire Line
	6750 5150 6750 4950
$Comp
L Device:R R77
U 1 1 5D6C25A7
P 6500 4850
F 0 "R77" V 6400 4800 50  0000 L CNN
F 1 "220R" V 6500 4750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6430 4850 50  0001 C CNN
F 3 "~" H 6500 4850 50  0001 C CNN
	1    6500 4850
	0    1    1    0   
$EndComp
$Comp
L Device:C C72
U 1 1 5D6C25AE
P 6200 5000
F 0 "C72" H 6315 5046 50  0000 L CNN
F 1 "0.1uF" H 6315 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 4850 50  0001 C CNN
F 3 "~" H 6200 5000 50  0001 C CNN
	1    6200 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4850 6350 4850
Wire Notes Line
	6050 4700 6050 5200
Wire Notes Line
	6050 5200 6700 5200
Wire Notes Line
	6700 5200 6700 4700
Wire Notes Line
	6700 4700 6050 4700
Text Notes 5950 4650 0    60   ~ 0
optional RC filter
Connection ~ 6200 4850
Wire Wire Line
	6050 4850 6200 4850
Text Notes 6900 4650 0    60   ~ 0
CV LFO1
$Comp
L Connector:AudioJack2 J80
U 1 1 5D6C9F94
P 6950 6300
F 0 "J80" H 6771 6375 50  0000 R CNN
F 1 "TS" H 6771 6284 50  0000 R CNN
F 2 "mod:CUI-MJ3507-bent" H 6950 6300 50  0001 C CNN
F 3 "~" H 6950 6300 50  0001 C CNN
	1    6950 6300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R86
U 1 1 5D6C9FF6
P 5900 6300
F 0 "R86" V 5800 6250 50  0000 L CNN
F 1 "1K" V 5900 6250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5830 6300 50  0001 C CNN
F 3 "~" H 5900 6300 50  0001 C CNN
	1    5900 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 6300 6750 6300
Wire Wire Line
	6750 6600 6750 6400
$Comp
L Device:R R87
U 1 1 5D6CA00E
P 6500 6300
F 0 "R87" V 6400 6250 50  0000 L CNN
F 1 "220R" V 6500 6200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6430 6300 50  0001 C CNN
F 3 "~" H 6500 6300 50  0001 C CNN
	1    6500 6300
	0    1    1    0   
$EndComp
$Comp
L Device:C C82
U 1 1 5D6CA015
P 6200 6450
F 0 "C82" H 6315 6496 50  0000 L CNN
F 1 "0.1uF" H 6315 6405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 6300 50  0001 C CNN
F 3 "~" H 6200 6450 50  0001 C CNN
	1    6200 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 6300 6350 6300
Wire Notes Line
	6050 6150 6050 6650
Wire Notes Line
	6700 6650 6700 6150
Wire Notes Line
	6700 6150 6050 6150
Text Notes 5950 6100 0    60   ~ 0
optional RC filter
Wire Wire Line
	5750 6300 5700 6300
Wire Wire Line
	5700 5950 5700 6300
Connection ~ 5700 6300
Wire Wire Line
	5700 6300 5450 6300
Connection ~ 6200 6300
Wire Wire Line
	6050 6300 6200 6300
Text Notes 6900 6100 0    60   ~ 0
CV LFO1
$Comp
L Connector:AudioJack2 J90
U 1 1 5D75E30D
P 6950 7750
F 0 "J90" H 6771 7825 50  0000 R CNN
F 1 "TS" H 6771 7734 50  0000 R CNN
F 2 "mod:CUI-MJ3507-bent" H 6950 7750 50  0001 C CNN
F 3 "~" H 6950 7750 50  0001 C CNN
	1    6950 7750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R96
U 1 1 5D75E36F
P 5900 7750
F 0 "R96" V 5800 7700 50  0000 L CNN
F 1 "1K" V 5900 7700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5830 7750 50  0001 C CNN
F 3 "~" H 5900 7750 50  0001 C CNN
	1    5900 7750
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 7750 6750 7750
Wire Wire Line
	6750 8050 6750 7850
$Comp
L Device:R R97
U 1 1 5D75E387
P 6500 7750
F 0 "R97" V 6400 7700 50  0000 L CNN
F 1 "220R" V 6500 7650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6430 7750 50  0001 C CNN
F 3 "~" H 6500 7750 50  0001 C CNN
	1    6500 7750
	0    1    1    0   
$EndComp
$Comp
L Device:C C92
U 1 1 5D75E38E
P 6200 7900
F 0 "C92" H 6315 7946 50  0000 L CNN
F 1 "0.1uF" H 6315 7855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 7750 50  0001 C CNN
F 3 "~" H 6200 7900 50  0001 C CNN
	1    6200 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 8050 6750 8050
Wire Wire Line
	6200 7750 6350 7750
Wire Notes Line
	6050 7600 6050 8100
Wire Notes Line
	6050 8100 6700 8100
Wire Notes Line
	6700 8100 6700 7600
Wire Notes Line
	6700 7600 6050 7600
Text Notes 5950 7550 0    60   ~ 0
optional RC filter
Wire Wire Line
	5750 7750 5700 7750
Wire Wire Line
	5700 7400 5700 7750
Connection ~ 6200 7750
Wire Wire Line
	6050 7750 6200 7750
Text Notes 6900 7550 0    60   ~ 0
CV LFO1
Wire Notes Line
	4950 8450 4950 2100
$Comp
L Device:D D1
U 1 1 5DAF7150
P 1550 6350
F 0 "D1" V 1550 6400 50  0000 L CNN
F 1 "1N4148" H 1400 6250 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 1550 6350 50  0001 C CNN
F 3 "~" H 1550 6350 50  0001 C CNN
	1    1550 6350
	-1   0    0    1   
$EndComp
$Comp
L Device:D D2
U 1 1 5DAF7877
P 1850 6100
F 0 "D2" V 1850 6150 50  0000 L CNN
F 1 "1N4148" H 1700 6000 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 1850 6100 50  0001 C CNN
F 3 "~" H 1850 6100 50  0001 C CNN
	1    1850 6100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5DAF91B3
P 1550 5750
F 0 "#PWR06" H 1550 5500 50  0001 C CNN
F 1 "GND" H 1555 5577 50  0000 C CNN
F 2 "" H 1550 5750 50  0001 C CNN
F 3 "" H 1550 5750 50  0001 C CNN
	1    1550 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 5750 1850 5950
Wire Wire Line
	1300 6300 1300 6350
Wire Wire Line
	1400 6350 1300 6350
Wire Wire Line
	1700 6350 1850 6350
Wire Wire Line
	1850 6350 1850 6400
Wire Wire Line
	1850 6250 1850 6350
Connection ~ 1850 6350
Wire Wire Line
	1850 6700 1850 6750
$Comp
L Connector:AudioJack2 J5
U 1 1 5DAF8911
P 1850 5550
F 0 "J5" V 1700 5650 50  0000 R CNN
F 1 "TS" V 1650 5500 50  0000 R CNN
F 2 "mod:CUI-MJ3507-bent" H 1850 5550 50  0001 C CNN
F 3 "~" H 1850 5550 50  0001 C CNN
	1    1850 5550
	0    -1   1    0   
$EndComp
Wire Wire Line
	1550 5750 1750 5750
$Comp
L Device:R R4
U 1 1 5DCEE5F5
P 2200 6350
F 0 "R4" V 2300 6300 50  0000 L CNN
F 1 "10K" V 2200 6250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2130 6350 50  0001 C CNN
F 3 "~" H 2200 6350 50  0001 C CNN
	1    2200 6350
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BCE Q1
U 1 1 5DCF4279
P 2600 6350
F 0 "Q1" H 2791 6396 50  0000 L CNN
F 1 "2N3904" H 2791 6305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92" H 2800 6450 50  0001 C CNN
F 3 "~" H 2600 6350 50  0001 C CNN
	1    2600 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5DD04858
P 2700 5900
F 0 "R5" V 2800 5850 50  0000 L CNN
F 1 "10K" V 2700 5800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2630 5900 50  0001 C CNN
F 3 "~" H 2700 5900 50  0001 C CNN
	1    2700 5900
	-1   0    0    1   
$EndComp
$Comp
L power:VDD #PWR016
U 1 1 5DD0620C
P 2700 5650
F 0 "#PWR016" H 2700 5500 50  0001 C CNN
F 1 "VDD" H 2717 5823 50  0000 C CNN
F 2 "" H 2700 5650 50  0001 C CNN
F 3 "" H 2700 5650 50  0001 C CNN
	1    2700 5650
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR03
U 1 1 5DD062FB
P 1300 5750
F 0 "#PWR03" H 1300 5600 50  0001 C CNN
F 1 "VDD" H 1317 5923 50  0000 C CNN
F 2 "" H 1300 5750 50  0001 C CNN
F 3 "" H 1300 5750 50  0001 C CNN
	1    1300 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5750 1300 5900
Wire Wire Line
	2700 5650 2700 5750
Wire Wire Line
	2700 6050 2700 6150
Wire Wire Line
	2400 6350 2350 6350
Wire Wire Line
	2050 6350 1850 6350
Text Notes 1700 5250 0    60   ~ 0
0;10V
Wire Wire Line
	2700 6550 2700 6750
Wire Wire Line
	2700 6750 1850 6750
Connection ~ 1850 6750
Wire Wire Line
	2700 6150 2950 6150
Connection ~ 2700 6150
$Comp
L Device:R R6
U 1 1 5DDAC744
P 3500 6500
F 0 "R6" H 3570 6546 50  0000 L CNN
F 1 "100K" V 3500 6400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3430 6500 50  0001 C CNN
F 3 "~" H 3500 6500 50  0001 C CNN
	1    3500 6500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5DDAC74B
P 3500 6700
F 0 "#PWR026" H 3500 6450 50  0001 C CNN
F 1 "GND" H 3505 6527 50  0000 C CNN
F 2 "" H 3500 6700 50  0001 C CNN
F 3 "" H 3500 6700 50  0001 C CNN
	1    3500 6700
	1    0    0    -1  
$EndComp
Text Label 4600 6100 0    60   ~ 0
CLK
$Comp
L Device:D D3
U 1 1 5DDAC761
P 3500 6050
F 0 "D3" V 3500 6100 50  0000 L CNN
F 1 "1N4148" H 3350 5950 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 3500 6050 50  0001 C CNN
F 3 "~" H 3500 6050 50  0001 C CNN
	1    3500 6050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5DDAC768
P 3200 5700
F 0 "#PWR021" H 3200 5450 50  0001 C CNN
F 1 "GND" H 3205 5527 50  0000 C CNN
F 2 "" H 3200 5700 50  0001 C CNN
F 3 "" H 3200 5700 50  0001 C CNN
	1    3200 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 5700 3500 5900
Wire Wire Line
	3500 6300 3500 6350
Wire Wire Line
	3500 6200 3500 6300
Connection ~ 3500 6300
Wire Wire Line
	3500 6650 3500 6700
$Comp
L Connector:AudioJack2 J6
U 1 1 5DDAC779
P 3500 5500
F 0 "J6" V 3350 5600 50  0000 R CNN
F 1 "TS" V 3300 5450 50  0000 R CNN
F 2 "mod:CUI-MJ3507-bent" H 3500 5500 50  0001 C CNN
F 3 "~" H 3500 5500 50  0001 C CNN
	1    3500 5500
	0    -1   1    0   
$EndComp
Wire Wire Line
	3200 5700 3400 5700
$Comp
L Device:R R7
U 1 1 5DDAC781
P 3850 6300
F 0 "R7" V 3950 6250 50  0000 L CNN
F 1 "10K" V 3850 6200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3780 6300 50  0001 C CNN
F 3 "~" H 3850 6300 50  0001 C CNN
	1    3850 6300
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BCE Q2
U 1 1 5DDAC788
P 4250 6300
F 0 "Q2" H 4441 6346 50  0000 L CNN
F 1 "2N3904" H 4441 6255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92" H 4450 6400 50  0001 C CNN
F 3 "~" H 4250 6300 50  0001 C CNN
	1    4250 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5DDAC78F
P 4350 5850
F 0 "R8" V 4450 5800 50  0000 L CNN
F 1 "10K" V 4350 5750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4280 5850 50  0001 C CNN
F 3 "~" H 4350 5850 50  0001 C CNN
	1    4350 5850
	-1   0    0    1   
$EndComp
$Comp
L power:VDD #PWR035
U 1 1 5DDAC796
P 4350 5600
F 0 "#PWR035" H 4350 5450 50  0001 C CNN
F 1 "VDD" H 4367 5773 50  0000 C CNN
F 2 "" H 4350 5600 50  0001 C CNN
F 3 "" H 4350 5600 50  0001 C CNN
	1    4350 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 5600 4350 5700
Wire Wire Line
	4350 6000 4350 6100
Wire Wire Line
	4050 6300 4000 6300
Wire Wire Line
	3700 6300 3500 6300
Text Notes 3350 5200 0    60   ~ 0
0;10V
Wire Wire Line
	4350 6500 4350 6700
Wire Wire Line
	4350 6700 3500 6700
Connection ~ 3500 6700
Wire Wire Line
	4350 6100 4600 6100
Connection ~ 4350 6100
Text Notes 3950 5300 0    60   ~ 0
CLOCK
Wire Notes Line
	850  5000 4850 5000
Wire Notes Line
	4850 5000 4850 7050
Wire Notes Line
	4850 7050 850  7050
Wire Notes Line
	850  7050 850  5000
Text Notes 950  5150 0    60   ~ 0
CV IN
$Comp
L Device:R R68
U 1 1 5D3DD077
P 6400 2850
F 0 "R68" V 6500 2800 50  0000 L CNN
F 1 "220R" V 6400 2750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 2850 50  0001 C CNN
F 3 "~" H 6400 2850 50  0001 C CNN
	1    6400 2850
	-1   0    0    -1  
$EndComp
Text Notes 7000 2600 0    60   ~ 0
WP57EYW \nVF=2V;2,1V\nR>150R
$Comp
L Device:LED_Dual_2pin D60
U 1 1 5D4E7471
P 6700 2700
F 0 "D60" H 6700 3050 50  0000 C CNN
F 1 "LED2" H 6700 2950 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6700 2700 50  0001 C CNN
F 3 "~" H 6700 2700 50  0001 C CNN
	1    6700 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D3DD070
P 7000 2700
F 0 "#PWR0104" H 7000 2450 50  0001 C CNN
F 1 "GND" H 7005 2527 50  0000 C CNN
F 2 "" H 7000 2700 50  0001 C CNN
F 3 "" H 7000 2700 50  0001 C CNN
	1    7000 2700
	0    -1   1    0   
$EndComp
Wire Wire Line
	5700 3000 6400 3000
$Comp
L Device:R R78
U 1 1 5D54ECEB
P 6250 4350
F 0 "R78" V 6350 4300 50  0000 L CNN
F 1 "220R" V 6250 4250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6180 4350 50  0001 C CNN
F 3 "~" H 6250 4350 50  0001 C CNN
	1    6250 4350
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED_Dual_2pin D70
U 1 1 5D54ECF3
P 6550 4200
F 0 "D70" H 6550 4550 50  0000 C CNN
F 1 "LED2" H 6550 4450 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6550 4200 50  0001 C CNN
F 3 "~" H 6550 4200 50  0001 C CNN
	1    6550 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5D54ECFA
P 6850 4200
F 0 "#PWR0105" H 6850 3950 50  0001 C CNN
F 1 "GND" H 6855 4027 50  0000 C CNN
F 2 "" H 6850 4200 50  0001 C CNN
F 3 "" H 6850 4200 50  0001 C CNN
	1    6850 4200
	0    -1   1    0   
$EndComp
$Comp
L Device:R R88
U 1 1 5D5899A2
P 6150 5800
F 0 "R88" V 6250 5750 50  0000 L CNN
F 1 "220R" V 6150 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6080 5800 50  0001 C CNN
F 3 "~" H 6150 5800 50  0001 C CNN
	1    6150 5800
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED_Dual_2pin D80
U 1 1 5D5899A9
P 6450 5650
F 0 "D80" H 6450 6000 50  0000 C CNN
F 1 "LED2" H 6450 5900 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6450 5650 50  0001 C CNN
F 3 "~" H 6450 5650 50  0001 C CNN
	1    6450 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5D5899B0
P 6750 5650
F 0 "#PWR0106" H 6750 5400 50  0001 C CNN
F 1 "GND" H 6755 5477 50  0000 C CNN
F 2 "" H 6750 5650 50  0001 C CNN
F 3 "" H 6750 5650 50  0001 C CNN
	1    6750 5650
	0    -1   1    0   
$EndComp
Wire Wire Line
	5700 5950 6150 5950
$Comp
L Device:R R98
U 1 1 5D5C3179
P 6300 7250
F 0 "R98" V 6400 7200 50  0000 L CNN
F 1 "220R" V 6300 7150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6230 7250 50  0001 C CNN
F 3 "~" H 6300 7250 50  0001 C CNN
	1    6300 7250
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED_Dual_2pin D90
U 1 1 5D5C3180
P 6600 7100
F 0 "D90" H 6600 7450 50  0000 C CNN
F 1 "LED2" H 6600 7350 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6600 7100 50  0001 C CNN
F 3 "~" H 6600 7100 50  0001 C CNN
	1    6600 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5D5C3187
P 6900 7100
F 0 "#PWR0107" H 6900 6850 50  0001 C CNN
F 1 "GND" H 6905 6927 50  0000 C CNN
F 2 "" H 6900 7100 50  0001 C CNN
F 3 "" H 6900 7100 50  0001 C CNN
	1    6900 7100
	0    -1   1    0   
$EndComp
Wire Wire Line
	6300 7400 5700 7400
Text Notes 6900 3650 0    60   ~ 0
MJ-3507
Text Label 5550 3350 2    60   ~ 0
LFO1
Text Label 5550 4850 2    60   ~ 0
LFO2
Text Label 5450 6300 2    60   ~ 0
LFO3
Text Label 5550 7750 2    60   ~ 0
LFO4
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5D36BF57
P 6750 9600
F 0 "H1" H 6850 9651 50  0000 L CNN
F 1 "M3" H 6850 9560 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 6750 9600 50  0001 C CNN
F 3 "~" H 6750 9600 50  0001 C CNN
	1    6750 9600
	1    0    0    -1  
$EndComp
NoConn ~ 6750 9700
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5D380E24
P 7050 9600
F 0 "H2" H 7150 9651 50  0000 L CNN
F 1 "M3" H 7150 9560 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 7050 9600 50  0001 C CNN
F 3 "~" H 7050 9600 50  0001 C CNN
	1    7050 9600
	1    0    0    -1  
$EndComp
NoConn ~ 7050 9700
Text Notes 2350 8350 0    60   ~ 0
SWITCHes
Text Notes 1300 8350 0    60   ~ 0
POTS
Text Notes 3600 7600 0    60   ~ 0
LFOs
Wire Wire Line
	3200 7750 3700 7750
Wire Wire Line
	3200 7850 3700 7850
Wire Wire Line
	3200 7950 3700 7950
Wire Wire Line
	3200 8050 3700 8050
Wire Wire Line
	3200 8150 3700 8150
Text Label 3700 7850 0    60   ~ 0
LFO3
Text Label 3700 7750 0    60   ~ 0
LFO4
Text Label 3700 7950 0    60   ~ 0
LFO2
Text Label 3700 8050 0    60   ~ 0
LFO1
Text Label 3700 8150 0    60   ~ 0
GND
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J9
U 1 1 5D84F0D1
P 3400 7950
F 0 "J9" H 3450 7525 50  0000 C CNN
F 1 "2x5" H 3450 7616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3400 7950 50  0001 C CNN
F 3 "~" H 3400 7950 50  0001 C CNN
	1    3400 7950
	1    0    0    1   
$EndComp
Wire Notes Line
	4850 4900 850  4900
Wire Notes Line
	850  2100 4850 2100
Text Label 1650 8150 0    60   ~ 0
GND
Text Label 2800 8150 0    60   ~ 0
GND
Text Label 1150 8150 2    60   ~ 0
GND
Text Label 1650 7750 0    60   ~ 0
VDD
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J7
U 1 1 5D6E87B2
P 1350 7950
F 0 "J7" H 1400 7525 50  0000 C CNN
F 1 "2x5" H 1400 7616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1350 7950 50  0001 C CNN
F 3 "~" H 1350 7950 50  0001 C CNN
	1    1350 7950
	1    0    0    1   
$EndComp
Text Label 2300 8150 2    60   ~ 0
GND
Text Label 2300 7750 2    60   ~ 0
VDD
Text Label 2800 8050 0    60   ~ 0
CLK
Text Label 2300 8050 2    60   ~ 0
INIT
Text Label 1150 7950 2    60   ~ 0
P0
Text Label 1150 8050 2    60   ~ 0
P1
Text Label 1650 8050 0    60   ~ 0
P2
Text Label 1650 7950 0    60   ~ 0
P3
Text Label 1650 7850 0    60   ~ 0
P4
Text Label 1150 7850 2    60   ~ 0
P5
Text Label 2300 7950 2    60   ~ 0
S1
Text Label 2300 7850 2    60   ~ 0
S2
Text Label 2800 7750 0    60   ~ 0
S3
Text Label 2800 7950 0    60   ~ 0
S4
Text Label 2800 7850 0    60   ~ 0
S5
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J8
U 1 1 5D623757
P 2500 7950
F 0 "J8" H 2550 7525 50  0000 C CNN
F 1 "2x5" H 2550 7616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2500 7950 50  0001 C CNN
F 3 "~" H 2500 7950 50  0001 C CNN
	1    2500 7950
	1    0    0    1   
$EndComp
Text Notes 1800 3150 1    60   ~ 0
Off-Mom
Text Notes 950  2250 0    60   ~ 0
INPUTS
Wire Notes Line
	850  4900 850  2100
Text Notes 3550 2350 0    60   ~ 0
LFO4
Text Notes 3000 2350 0    60   ~ 0
LFO3
Text Notes 2450 2350 0    60   ~ 0
LFO2
Text Notes 1900 2350 0    60   ~ 0
LFO1
Text Notes 4050 2350 0    60   ~ 0
MODE
Wire Wire Line
	4350 2950 4350 3000
Wire Wire Line
	4150 2950 4350 2950
Text Label 4350 3000 0    60   ~ 0
S5
Wire Wire Line
	3850 2950 3850 3000
Wire Wire Line
	3650 2950 3850 2950
Wire Wire Line
	3300 2950 3300 3000
Wire Wire Line
	3100 2950 3300 2950
Wire Wire Line
	2750 2950 2750 3000
Wire Wire Line
	2550 2950 2750 2950
Wire Wire Line
	2200 2950 2200 3000
Wire Wire Line
	2000 2950 2200 2950
Text Label 3850 3000 0    60   ~ 0
S4
Text Label 3300 3000 0    60   ~ 0
S3
Text Label 2750 3000 0    60   ~ 0
S2
Text Label 2200 3000 0    60   ~ 0
S1
$Comp
L power:GND #PWR028
U 1 1 5D3CF642
P 4150 3350
F 0 "#PWR028" H 4150 3100 50  0001 C CNN
F 1 "GND" H 4155 3177 50  0000 C CNN
F 2 "" H 4150 3350 50  0001 C CNN
F 3 "" H 4150 3350 50  0001 C CNN
	1    4150 3350
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR027
U 1 1 5D3CF427
P 4150 2650
F 0 "#PWR027" H 4150 2500 50  0001 C CNN
F 1 "VDD" H 4167 2823 50  0000 C CNN
F 2 "" H 4150 2650 50  0001 C CNN
F 3 "" H 4150 2650 50  0001 C CNN
	1    4150 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R55
U 1 1 5D3CF2FD
P 4150 2800
F 0 "R55" H 4220 2846 50  0000 L CNN
F 1 "10k" V 4150 2750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4080 2800 50  0001 C CNN
F 3 "~" H 4150 2800 50  0001 C CNN
	1    4150 2800
	1    0    0    -1  
$EndComp
Connection ~ 4150 2950
$Comp
L Switch:SW_SPST SW5
U 1 1 5D3CD172
P 4150 3150
F 0 "SW5" V 4104 3248 50  0000 L CNN
F 1 "TOGGLE" V 4195 3248 50  0000 L CNN
F 2 "mod:Toggle-MTS-1" H 4150 3150 50  0001 C CNN
F 3 "" H 4150 3150 50  0001 C CNN
	1    4150 3150
	0    1    1    0   
$EndComp
$Comp
L power:VDD #PWR029
U 1 1 5D3BF60A
P 4200 4100
F 0 "#PWR029" H 4200 3950 50  0001 C CNN
F 1 "VDD" H 4217 4273 50  0000 C CNN
F 2 "" H 4200 4100 50  0001 C CNN
F 3 "" H 4200 4100 50  0001 C CNN
	1    4200 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR08
U 1 1 5D396C68
P 2000 2650
F 0 "#PWR08" H 2000 2500 50  0001 C CNN
F 1 "VDD" H 2017 2823 50  0000 C CNN
F 2 "" H 2000 2650 50  0001 C CNN
F 3 "" H 2000 2650 50  0001 C CNN
	1    2000 2650
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR012
U 1 1 5D39D858
P 2550 2650
F 0 "#PWR012" H 2550 2500 50  0001 C CNN
F 1 "VDD" H 2567 2823 50  0000 C CNN
F 2 "" H 2550 2650 50  0001 C CNN
F 3 "" H 2550 2650 50  0001 C CNN
	1    2550 2650
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR017
U 1 1 5D39F8DF
P 3100 2650
F 0 "#PWR017" H 3100 2500 50  0001 C CNN
F 1 "VDD" H 3117 2823 50  0000 C CNN
F 2 "" H 3100 2650 50  0001 C CNN
F 3 "" H 3100 2650 50  0001 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Text Notes 4100 3850 0    60   ~ 0
WAVE
Text Label 4350 4250 0    60   ~ 0
P5
$Comp
L power:GND #PWR030
U 1 1 5D3A4109
P 4200 4400
F 0 "#PWR030" H 4200 4150 50  0001 C CNN
F 1 "GND" H 4205 4227 50  0000 C CNN
F 2 "" H 4200 4400 50  0001 C CNN
F 3 "" H 4200 4400 50  0001 C CNN
	1    4200 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV5
U 1 1 5D3A4102
P 4200 4250
F 0 "RV5" H 4130 4204 50  0000 R CNN
F 1 "10k" H 4130 4295 50  0000 R CNN
F 2 "mod:Bourns-PTA2043" H 4200 4250 50  0001 C CNN
F 3 "~" H 4200 4250 50  0001 C CNN
	1    4200 4250
	1    0    0    1   
$EndComp
$Comp
L power:VDD #PWR022
U 1 1 5D3A1952
P 3650 2650
F 0 "#PWR022" H 3650 2500 50  0001 C CNN
F 1 "VDD" H 3667 2823 50  0000 C CNN
F 2 "" H 3650 2650 50  0001 C CNN
F 3 "" H 3650 2650 50  0001 C CNN
	1    3650 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R54
U 1 1 5D3A194B
P 3650 2800
F 0 "R54" H 3720 2846 50  0000 L CNN
F 1 "10k" V 3650 2750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3580 2800 50  0001 C CNN
F 3 "~" H 3650 2800 50  0001 C CNN
	1    3650 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5D3A1945
P 3650 3350
F 0 "#PWR023" H 3650 3100 50  0001 C CNN
F 1 "GND" H 3655 3177 50  0000 C CNN
F 2 "" H 3650 3350 50  0001 C CNN
F 3 "" H 3650 3350 50  0001 C CNN
	1    3650 3350
	1    0    0    -1  
$EndComp
Connection ~ 3650 2950
$Comp
L Switch:SW_Push SW4
U 1 1 5D3A193E
P 3650 3150
F 0 "SW4" V 3604 3298 50  0000 L CNN
F 1 "PUSH" V 3695 3298 50  0000 L CNN
F 2 "mod:Push-PS1024" H 3650 3350 50  0001 C CNN
F 3 "" H 3650 3350 50  0001 C CNN
	1    3650 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R53
U 1 1 5D39F8D8
P 3100 2800
F 0 "R53" H 3170 2846 50  0000 L CNN
F 1 "10k" V 3100 2750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3030 2800 50  0001 C CNN
F 3 "~" H 3100 2800 50  0001 C CNN
	1    3100 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5D39F8D2
P 3100 3350
F 0 "#PWR018" H 3100 3100 50  0001 C CNN
F 1 "GND" H 3105 3177 50  0000 C CNN
F 2 "" H 3100 3350 50  0001 C CNN
F 3 "" H 3100 3350 50  0001 C CNN
	1    3100 3350
	1    0    0    -1  
$EndComp
Connection ~ 3100 2950
$Comp
L Switch:SW_Push SW3
U 1 1 5D39F8CB
P 3100 3150
F 0 "SW3" V 3054 3298 50  0000 L CNN
F 1 "PUSH" V 3145 3298 50  0000 L CNN
F 2 "mod:Push-PS1024" H 3100 3350 50  0001 C CNN
F 3 "" H 3100 3350 50  0001 C CNN
	1    3100 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R52
U 1 1 5D39D851
P 2550 2800
F 0 "R52" H 2620 2846 50  0000 L CNN
F 1 "10k" V 2550 2750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2480 2800 50  0001 C CNN
F 3 "~" H 2550 2800 50  0001 C CNN
	1    2550 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5D39D84B
P 2550 3350
F 0 "#PWR013" H 2550 3100 50  0001 C CNN
F 1 "GND" H 2555 3177 50  0000 C CNN
F 2 "" H 2550 3350 50  0001 C CNN
F 3 "" H 2550 3350 50  0001 C CNN
	1    2550 3350
	1    0    0    -1  
$EndComp
Connection ~ 2550 2950
$Comp
L Switch:SW_Push SW2
U 1 1 5D39D844
P 2550 3150
F 0 "SW2" V 2504 3298 50  0000 L CNN
F 1 "PUSH" V 2595 3298 50  0000 L CNN
F 2 "mod:Push-PS1024" H 2550 3350 50  0001 C CNN
F 3 "" H 2550 3350 50  0001 C CNN
	1    2550 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R51
U 1 1 5D396BDE
P 2000 2800
F 0 "R51" H 2070 2846 50  0000 L CNN
F 1 "10k" V 2000 2750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1930 2800 50  0001 C CNN
F 3 "~" H 2000 2800 50  0001 C CNN
	1    2000 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5D396B7F
P 2000 3350
F 0 "#PWR09" H 2000 3100 50  0001 C CNN
F 1 "GND" H 2005 3177 50  0000 C CNN
F 2 "" H 2000 3350 50  0001 C CNN
F 3 "" H 2000 3350 50  0001 C CNN
	1    2000 3350
	1    0    0    -1  
$EndComp
Connection ~ 2000 2950
$Comp
L Switch:SW_Push SW1
U 1 1 5D39658A
P 2000 3150
F 0 "SW1" V 1954 3298 50  0000 L CNN
F 1 "PUSH" V 2045 3298 50  0000 L CNN
F 2 "mod:Push-PS1024" H 2000 3350 50  0001 C CNN
F 3 "" H 2000 3350 50  0001 C CNN
	1    2000 3150
	0    1    1    0   
$EndComp
Text Notes 1300 3850 0    60   ~ 0
CLOCK
Text Notes 3550 3850 0    60   ~ 0
LFO4
Text Notes 3000 3850 0    60   ~ 0
LFO3
Text Notes 2450 3850 0    60   ~ 0
LFO2
Text Notes 1900 3850 0    60   ~ 0
LFO1
$Comp
L power:VDD #PWR024
U 1 1 5D30881F
P 3650 4100
F 0 "#PWR024" H 3650 3950 50  0001 C CNN
F 1 "VDD" H 3667 4273 50  0000 C CNN
F 2 "" H 3650 4100 50  0001 C CNN
F 3 "" H 3650 4100 50  0001 C CNN
	1    3650 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR019
U 1 1 5D3087CE
P 3100 4100
F 0 "#PWR019" H 3100 3950 50  0001 C CNN
F 1 "VDD" H 3117 4273 50  0000 C CNN
F 2 "" H 3100 4100 50  0001 C CNN
F 3 "" H 3100 4100 50  0001 C CNN
	1    3100 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR014
U 1 1 5D30877D
P 2550 4100
F 0 "#PWR014" H 2550 3950 50  0001 C CNN
F 1 "VDD" H 2567 4273 50  0000 C CNN
F 2 "" H 2550 4100 50  0001 C CNN
F 3 "" H 2550 4100 50  0001 C CNN
	1    2550 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR010
U 1 1 5D30872C
P 2000 4100
F 0 "#PWR010" H 2000 3950 50  0001 C CNN
F 1 "VDD" H 2017 4273 50  0000 C CNN
F 2 "" H 2000 4100 50  0001 C CNN
F 3 "" H 2000 4100 50  0001 C CNN
	1    2000 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR04
U 1 1 5D3085B4
P 1450 4100
F 0 "#PWR04" H 1450 3950 50  0001 C CNN
F 1 "VDD" H 1467 4273 50  0000 C CNN
F 2 "" H 1450 4100 50  0001 C CNN
F 3 "" H 1450 4100 50  0001 C CNN
	1    1450 4100
	1    0    0    -1  
$EndComp
Text Label 1600 4250 0    60   ~ 0
P0
$Comp
L power:GND #PWR05
U 1 1 5D3069EB
P 1450 4400
F 0 "#PWR05" H 1450 4150 50  0001 C CNN
F 1 "GND" H 1455 4227 50  0000 C CNN
F 2 "" H 1450 4400 50  0001 C CNN
F 3 "" H 1450 4400 50  0001 C CNN
	1    1450 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV0
U 1 1 5D3069DE
P 1450 4250
F 0 "RV0" H 1380 4204 50  0000 R CNN
F 1 "10k" H 1380 4295 50  0000 R CNN
F 2 "mod:Pot-P100KH1" H 1450 4250 50  0001 C CNN
F 3 "~" H 1450 4250 50  0001 C CNN
	1    1450 4250
	1    0    0    1   
$EndComp
Text Label 3800 4250 0    60   ~ 0
P4
$Comp
L power:GND #PWR025
U 1 1 5D34CC99
P 3650 4400
F 0 "#PWR025" H 3650 4150 50  0001 C CNN
F 1 "GND" H 3655 4227 50  0000 C CNN
F 2 "" H 3650 4400 50  0001 C CNN
F 3 "" H 3650 4400 50  0001 C CNN
	1    3650 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV4
U 1 1 5D34CC8C
P 3650 4250
F 0 "RV4" H 3580 4204 50  0000 R CNN
F 1 "10k" H 3580 4295 50  0000 R CNN
F 2 "mod:Pot-P100KH1" H 3650 4250 50  0001 C CNN
F 3 "~" H 3650 4250 50  0001 C CNN
	1    3650 4250
	1    0    0    1   
$EndComp
Text Label 3250 4250 0    60   ~ 0
P3
$Comp
L power:GND #PWR020
U 1 1 5D348820
P 3100 4400
F 0 "#PWR020" H 3100 4150 50  0001 C CNN
F 1 "GND" H 3105 4227 50  0000 C CNN
F 2 "" H 3100 4400 50  0001 C CNN
F 3 "" H 3100 4400 50  0001 C CNN
	1    3100 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV3
U 1 1 5D348813
P 3100 4250
F 0 "RV3" H 3030 4204 50  0000 R CNN
F 1 "10k" H 3030 4295 50  0000 R CNN
F 2 "mod:Pot-P100KH1" H 3100 4250 50  0001 C CNN
F 3 "~" H 3100 4250 50  0001 C CNN
	1    3100 4250
	1    0    0    1   
$EndComp
Text Label 2700 4250 0    60   ~ 0
P2
$Comp
L power:GND #PWR015
U 1 1 5D3443B3
P 2550 4400
F 0 "#PWR015" H 2550 4150 50  0001 C CNN
F 1 "GND" H 2555 4227 50  0000 C CNN
F 2 "" H 2550 4400 50  0001 C CNN
F 3 "" H 2550 4400 50  0001 C CNN
	1    2550 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV2
U 1 1 5D3443A6
P 2550 4250
F 0 "RV2" H 2480 4204 50  0000 R CNN
F 1 "10k" H 2480 4295 50  0000 R CNN
F 2 "mod:Pot-P100KH1" H 2550 4250 50  0001 C CNN
F 3 "~" H 2550 4250 50  0001 C CNN
	1    2550 4250
	1    0    0    1   
$EndComp
Text Label 2150 4250 0    60   ~ 0
P1
$Comp
L power:GND #PWR011
U 1 1 5D33B227
P 2000 4400
F 0 "#PWR011" H 2000 4150 50  0001 C CNN
F 1 "GND" H 2005 4227 50  0000 C CNN
F 2 "" H 2000 4400 50  0001 C CNN
F 3 "" H 2000 4400 50  0001 C CNN
	1    2000 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5D32D6B9
P 2000 4250
F 0 "RV1" H 1930 4204 50  0000 R CNN
F 1 "10k" H 1930 4295 50  0000 R CNN
F 2 "mod:Pot-P100KH1" H 2000 4250 50  0001 C CNN
F 3 "~" H 2000 4250 50  0001 C CNN
	1    2000 4250
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D3BF3BC
P 5750 5150
F 0 "#PWR0101" H 5750 4900 50  0001 C CNN
F 1 "GND" H 5755 4977 50  0000 C CNN
F 2 "" H 5750 5150 50  0001 C CNN
F 3 "" H 5750 5150 50  0001 C CNN
	1    5750 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 5150 6200 5150
Connection ~ 6200 5150
Wire Wire Line
	6200 5150 6750 5150
Wire Wire Line
	6250 4500 5750 4500
Wire Wire Line
	5750 4500 5750 4850
Wire Wire Line
	5550 4850 5750 4850
Connection ~ 5750 4850
$Comp
L power:GND #PWR0102
U 1 1 5D3D98C9
P 5700 3650
F 0 "#PWR0102" H 5700 3400 50  0001 C CNN
F 1 "GND" H 5705 3477 50  0000 C CNN
F 2 "" H 5700 3650 50  0001 C CNN
F 3 "" H 5700 3650 50  0001 C CNN
	1    5700 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 3650 5700 3650
Connection ~ 6200 3650
$Comp
L power:GND #PWR0103
U 1 1 5D4208FA
P 5700 6600
F 0 "#PWR0103" H 5700 6350 50  0001 C CNN
F 1 "GND" H 5705 6427 50  0000 C CNN
F 2 "" H 5700 6600 50  0001 C CNN
F 3 "" H 5700 6600 50  0001 C CNN
	1    5700 6600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6750 6600 6200 6600
Connection ~ 6200 6600
Wire Wire Line
	6200 6600 5700 6600
Wire Wire Line
	5700 7750 5550 7750
Connection ~ 5700 7750
$Comp
L power:GND #PWR0108
U 1 1 5D449F36
P 5700 8050
F 0 "#PWR0108" H 5700 7800 50  0001 C CNN
F 1 "GND" H 5705 7877 50  0000 C CNN
F 2 "" H 5700 8050 50  0001 C CNN
F 3 "" H 5700 8050 50  0001 C CNN
	1    5700 8050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 8050 5700 8050
Connection ~ 6200 8050
Wire Notes Line
	4950 2100 7650 2100
Wire Notes Line
	7650 2100 7650 8450
Wire Notes Line
	7650 8450 4950 8450
Text Notes 5050 2250 0    50   ~ 0
CV OUT
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5D491C2C
P 4600 7900
F 0 "#FLG0101" H 4600 7975 50  0001 C CNN
F 1 "PWR_FLAG" H 4600 8074 50  0000 C CNN
F 2 "" H 4600 7900 50  0001 C CNN
F 3 "~" H 4600 7900 50  0001 C CNN
	1    4600 7900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5D491EC7
P 4600 7900
F 0 "#PWR0109" H 4600 7650 50  0001 C CNN
F 1 "GND" H 4605 7727 50  0000 C CNN
F 2 "" H 4600 7900 50  0001 C CNN
F 3 "" H 4600 7900 50  0001 C CNN
	1    4600 7900
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0110
U 1 1 5D492247
P 4200 7900
F 0 "#PWR0110" H 4200 7750 50  0001 C CNN
F 1 "VDD" H 4217 8073 50  0000 C CNN
F 2 "" H 4200 7900 50  0001 C CNN
F 3 "" H 4200 7900 50  0001 C CNN
	1    4200 7900
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5D49233E
P 4200 7900
F 0 "#FLG0102" H 4200 7975 50  0001 C CNN
F 1 "PWR_FLAG" H 4200 8073 50  0000 C CNN
F 2 "" H 4200 7900 50  0001 C CNN
F 3 "~" H 4200 7900 50  0001 C CNN
	1    4200 7900
	-1   0    0    1   
$EndComp
Text Label 1150 7750 2    60   ~ 0
VDD
Text Notes 6250 8200 0    50   ~ 0
Tau=RC=22us
Wire Notes Line
	4850 2100 4850 4900
Wire Notes Line
	850  7150 4850 7150
Wire Notes Line
	4850 7150 4850 8450
Wire Notes Line
	4850 8450 850  8450
Wire Notes Line
	850  8450 850  7150
Text Notes 950  7300 0    60   ~ 0
HEADERS
Text Notes 1200 7000 0    197  ~ 0
WARNING ! WRONG BJT's \nNPN transistor footprint\nE-B-C, not B-C-E !!!!
Text Notes 5000 9100 0    50   ~ 0
NB: at 160pm and 1/32, half a square wave lasts for about 23ms\n4QN/(160bpm/60)=1.5 seconds per bar; >>1500ms/64 = 23ms  \nI thought Tau=2.2ms (C=10uF) would be enough..\nthough tested with 10uF and output only reaches 80% \nof the 10V sweep over the 23ms half period\nwill try 1uF and see
$EndSCHEMATC
